/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Framework Post
 * 
 *
 */
qx.Class.define("ic.io.framework.Post", {
	extend: qx.io.request.Xhr,

	construct: function(url, data) {

		this.base(arguments, url);

		this.setMethod('POST');
		this.setAccept('application/json');
		this.setRequestHeader('x-framework-json-request', 'PLAINJSON');
		this.setParser("json");
		this.setRequestData(data);

	}

});
