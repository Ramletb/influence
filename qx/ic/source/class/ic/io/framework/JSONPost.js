/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Framework JSON Post
 * 
 *
 */
qx.Class.define("ic.io.framework.JSONPost", {
	extend: qx.io.request.Xhr,

	construct: function(fwModel, json, remove) {

		remove = remove ? 1 : 0;

		this.base(arguments, "/admin/jsonDataPost/remove/" + remove + "/model/" + fwModel + ".json");

		this.setMethod('POST');
		this.setAccept('application/json');
		this.setRequestHeader('x-framework-json-request', 'PLAINJSON');
		this.setParser("json");
		this.setRequestData({ "json": json });

	}

});
