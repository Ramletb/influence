/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Framework Request
 * 
 *
 */
qx.Class.define("ic.io.framework.Request", {
	extend: qx.io.request.Xhr,

	construct: function(fwModel, api, page, perPage) {
		page    = page    ? page    : 1;
		perPage = perPage ? perPage : 1000;

		var url = "/admin/getModelData/model/" + fwModel + ic.io.framework.API.encode(api) + "/perPage/" + perPage + "/page/" + page + ".json";

		this.base(arguments, url);

		this.setMethod('GET');
		this.setAccept('application/json');
		this.setRequestHeader('x-framework-json-request', 'PLAINJSON');
		this.setParser("json");
	}

});
