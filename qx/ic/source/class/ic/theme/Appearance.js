/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Appearances
 */
qx.Theme.define("ic.theme.Appearance", {
	extend : qx.theme.modern.Appearance,

	appearances : {

		"vt-app-header" : {
			style : function(states) {
				return {
					font : "bold",
					textColor : "text-selected",
					padding : [8,8,8,8],
					//decorator : "vt-app-header"
					decorator : "app-header"
				};
			}
		}
	}

});