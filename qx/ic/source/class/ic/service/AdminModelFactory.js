/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Framework Table Model
 *
 *
 */
qx.Class.define("ic.service.AdminModelFactory", {
	type: "static",

	statics: {

		getModelConfig: function() {
			return {
				"Page":
					{
						id: "ID",
						title: "Title",
						description: "Description"
					},
				"Content":
					{
						id: "ID",
						value: "Value"
					},
				"VwPageContent":
					{
						page_id: "Page ID",
						content_id: "Content ID",
						content_value: "Value"
					}
			};
		},

		getServiceUrl: function() {
			return "/admin/";
		},

		getModelSkel: function(model) {
			var m = this.getModelConfig()[model];
			var skel = {};
			for(var k in m) {
				skel[k] = null;
			}
			return skel;
		},

		getModel: function(model) {
			return new ic.io.framework.TableModel(this.getServiceUrl(), model, this.getModelConfig()[model]);
		}

	}

});