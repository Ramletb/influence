/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/arrow_refresh.png)

************************************************************************ */

/**
 * Wysiwyg Editor Window
 * 
 *
 */
qx.Class.define("ic.ui.window.WysiwygEditor", {
	extend: ic.ui.Window,

	construct: function(caption, icon) {
		this.base(arguments, caption, icon);

		this.__editor = new ic.ui.widget.WysiwygEditor();

		this.add(this.__editor, { row: 1, column: 0 });
	},

	members: {

		__editor: null,

		/**
		 * Set Content
		 *
		 * 
		 *
		 * @param content {String} content
		 */
		setContent: function(content) {
			this.__editor.setContent(content);
		}

	}

});
