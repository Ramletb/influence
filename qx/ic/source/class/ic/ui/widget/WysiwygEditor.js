/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/*)
#asset(ic/editor/*)

************************************************************************ */

/**
 * Wysiwyg Editor
 * 
 *
 */
qx.Class.define("ic.ui.widget.WysiwygEditor", {
	extend: qx.ui.container.Composite,

	construct: function() {

		this.base(arguments);

		// layout
		var layout = new qx.ui.layout.VBox();
		this.setLayout(layout);

		var htmlDecorator = new qx.ui.decoration.Single(1, "solid", "border-main");
		htmlDecorator.setWidthTop(0);

		this.__htmlArea = new qx.ui.embed.HtmlArea(null,  { "p": "padding-bottom:10px" }, null);
		this.__htmlArea.set( { width: 600, height: 400 /*, decorator: htmlDecorator */ } );

		this.__htmlArea.fontColorChooser = new qx.ui.control.ColorPopup();
		this.__htmlArea.bgColorChooser = new qx.ui.control.ColorPopup();

		this.__htmlArea.fontColorChooser.addListener("changeValue", function(e) {
			this.setTextColor(this.fontColorChooser.getValue());
		}, this.__htmlArea);

		this.__htmlArea.bgColorChooser.addListener("changeValue", function(e) {
			this.setTextBackgroundColor(this.bgColorChooser.getValue());
		}, this.__htmlArea);


		// Add Toolbar
		this.add(this.__setupToolbar());

		// Add Editing Area
		this.add(this.__htmlArea);

		var test = new qx.ui.form.Button("Test").set({ 
			allowGrowX: false,
			width: 100
		});
		test.addListener("execute", function(e) {
			this.debug(this.__htmlArea.getHtml());
		}, this);

		this.add(test);

	},

	members: {
		__htmlArea : null,

		/**
		 * Set Content
		 *
		 * 
		 *
		 * @param htmlContent {String} html content
		 */
		setContent: function(htmlContent) {
			this.__htmlArea.setValue(htmlContent)
		},

		/**
		* Handler method for font color
		*
		* @param e {qx.event.type.Event} event instance
		*/
		__fontColorHandler : function(e) {
			this.fontColorChooser.placeToWidget(e.getTarget());
			this.fontColorChooser.show();
		},

		/**
		* Handler method for text background color
		*
		* @param e {qx.event.type.Event} event instance
		*/
		__textBackgroundColorHandler : function(e) {
			this.bgColorChooser.placeToWidget(e.getTarget());
			this.bgColorChooser.show();
		},

		/**
		* Handler method for inserting images
		*
		* @param e {qx.event.type.Event} event instance
		*/
		__insertImageHandler : function(e) {
			var attributes = { 
				src    : qx.util.ResourceManager.getInstance().toUri("demobrowser/demo/icons/htmlarea/qooxdoo_logo.png"),
				border : 0,
				title  : "qooxdoo logo",
				alt    : "qooxdoo logo" };

			this.insertImage(attributes);
	    },

		/**
		* Handler method for inserting tables
		*
		* @param e {qx.event.type.Event} event instance
		*/
		__insertTableHandler : function(e) {
			var table = "<table border='1'>" +
	                    "<tbody>" +
	                      "<tr>" +
	                        "<td>First Row, First cell</td>" +
	                        "<td>First Row, Second cell</td>" +
	                      "</tr>" +
	                      "<tr>" +
	                        "<td>Second Row, First cell</td>" +
	                        "<td>Second Row, Second cell</td>" +
	                      "</tr>" +
	                    "</tbody>" +
	                  "</table>";
			this.insertHtml(table);
		},

		/**
		* Handler method for inserting links
		*
		* @param e {qx.event.type.Event} event instance
		*/
		__insertLinkHandler : function(e) {
			var createLinkWindow = new qx.ui.window.Window("Insert Hyperlink");
			createLinkWindow.setLayout(new qx.ui.layout.VBox(20));
			createLinkWindow.set({ width: 400, showMaximize: false, showMinimize: false });

			var textField = new qx.ui.form.TextField("http://");
			createLinkWindow.add(textField);

			var hBoxLayout = new qx.ui.layout.HBox(10);
			hBoxLayout.setAlignX("right");
			var buttonContainer = new qx.ui.container.Composite(hBoxLayout);

			var okButton = new qx.ui.form.Button("OK");
			okButton.setWidth(60);
			okButton.addListener("execute", function(e) {
				this.insertHyperLink(textField.getValue());
				createLinkWindow.close();
			}, this);
			buttonContainer.add(okButton);

			var cancelButton = new qx.ui.form.Button("Cancel");
			cancelButton.setWidth(60);
			cancelButton.addListener("execute", function(e) {
				createLinkWindow.close();
			}, this);
			buttonContainer.add(cancelButton);

			createLinkWindow.add(buttonContainer);

			createLinkWindow.center();
			createLinkWindow.open();

			this.saveRange();
	    },

		/**
		* Handler method for inserting HTML code
		*
		* @param e {qx.event.type.Event} event instance
		*/
		__insertHTMLHandler : function(e) {
			var result = window.prompt("HTML Code:", "");
			this.insertHtml(result);
		},



		/**
		* Toolbar entries
		*
		* @return {Array} toolbar entries
		*/
		__getToolbarEntries : function() {
			return [
			{
				bold:                { text: "Format Bold", image: "ic/editor/text_bold.png", action: this.__htmlArea.setBold },
				italic:              { text: "Format Italic", image: "ic/editor/text_italic.png", action: this.__htmlArea.setItalic },
				underline:           { text: "Format Underline", image: "ic/editor/text_underline.png", action: this.__htmlArea.setUnderline },
				strikethrough:       { text: "Format Strikethrough", image: "ic/editor/text_strikethrough.png", action: this.__htmlArea.setStrikeThrough }
				//removeFormat:        { text: "Remove Format", image: "qx/icon/Oxygen/16/actions/edit-clear.png", action: this.__htmlArea.removeFormat }
			},

			{
				alignLeft:           { text: "Align Left", image: "ic/editor/text_align_left.png", action: this.__htmlArea.setJustifyLeft },
				alignCenter:         { text: "Align Center", image: "ic/editor/text_align_center.png", action: this.__htmlArea.setJustifyCenter },
				alignRight:          { text: "Align Right", image: "ic/editor/text_align_right.png", action: this.__htmlArea.setJustifyRight },
				alignJustify:        { text: "Align Justify", image: "ic/editor/text_align_justify.png", action: this.__htmlArea.setJustifyFull }
			},

			{
				//fontFamily:          { custom: this.__fontFamilyToolbarEntry },
				//fontSize:            { custom: this.__fontSizeToolbarEntry },
				fontColor:           { text: "Set Text Color", image:  "ic/color_wheel.png", action: this.__fontColorHandler },
				textBackgroundColor: { text: "Set Text Background Color", image:  "ic/color_swatch.png", action: this.__textBackgroundColorHandler }
			},

			{
				indent:              { text: "Indent More", image: "ic/editor/text_indent.png", action: this.__htmlArea.insertIndent },
				outdent:             { text: "Indent Less", image: "ic/editor/text_indent_remove.png", action: this.__htmlArea.insertOutdent }
			},

			{
				//insertImage:         { text: "Insert Image", image: "qx/icon/Oxygen/16/actions/insert-image.png", action: this.__insertImageHandler },
				//insertTable:         { text: "Insert Table", image: "ic/editor/table.png", action: this.__insertTableHandler },
				insertLink:          { text: "Insert Link", image: "ic/editor/link.png", action: this.__insertLinkHandler },
				insertHTML:          { text: "Insert HTML Code", image: "ic/editor/tag.png", action: this.__insertHTMLHandler },
				insertHR:            { text: "Insert Horizontal Ruler", image: "ic/editor/hr.png", action: this.__htmlArea.insertHorizontalRuler }
			},

			{
				ol:                  { text: "Insert Ordered List", image: "ic/editor/text_list_numbers.png", action: this.__htmlArea.insertOrderedList },
				ul:                  { text: "Inserted Unordered List", image: "ic/editor/text_list_bullets.png", action: this.__htmlArea.insertUnorderedList }
			}
/*
			{
				undo:                { text: "Undo Last Change", image: "qx/icon/Oxygen/16/actions/edit-undo.png", action: this.__htmlArea.undo },
				redo:                { text: "Redo Last Undo Step", image: "qx/icon/Oxygen/16/actions/edit-redo.png", action: this.__htmlArea.redo }
			}
*/
			];
		},

		/**
		* Creates the toolbar entries
		*
		* @return {qx.ui.toolbarToolBar} toolbar widget
		*/
		__setupToolbar : function() {
			var toolbar = new qx.ui.toolbar.ToolBar();
			toolbar.setDecorator("main");

			// Put together toolbar entries
			var button;
			var toolbarEntries = this.__getToolbarEntries();
			for (var i=0, j=toolbarEntries.length; i<j; i++) {
				var part = new qx.ui.toolbar.Part;
				toolbar.add(part);

				for (var entry in toolbarEntries[i]) {
					var infos = toolbarEntries[i][entry];

					if(infos.custom) {
						button = infos.custom.call(this);
					} else {
						button = new qx.ui.toolbar.Button(null, infos.image);
						button.set({ focusable : false,
							keepFocus : true,
							center : true,
							toolTipText : infos.text ? infos.text : "" });
							button.addListener("execute", infos.action, this.__htmlArea);
					}
					part.add(button);
				}
			}

			return toolbar;
		}

	}

});
