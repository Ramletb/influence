/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/arrow_refresh.png)
#asset(ic/add.png)
#asset(ic/page_white_text.png)

************************************************************************ */

/**
 * Data Window
 * 
 *
 */
qx.Class.define("ic.app.page.window.DataTable", {
	extend: ic.ui.window.DataTable,

	construct: function() {
		this.__type  = "Page";
		this.__icon  = null;

		this.base(arguments, "Pages", "ic/page_white_text.png");


		var tcm = this.getTableColumnModel();
		//tcm.setColumnVisible(0, false);

		var resizeBehavior = tcm.getBehavior();
		resizeBehavior.set(0, { width:"110", minWidth:110, maxWidth:110 });
		resizeBehavior.set(1, { width:"110", minWidth:110, maxWidth:110 });

		var table = this.getTable();

		table.addListener("cellDblclick", function(e) {
			var row = table.getTableModel().getRowData(e.getRow());
			var win = new ic.app.page.window.Editor();
			this.__populate(win, row);
			win.open();

			win.addListener("recordUpdate", function(e) {
				this.getTable().getTableModel().reloadData();
			}, this);
		}, this);

	},

	members: {

		__type: null,

		__icon: null,

		/**
		 * Populate
		 *
		 * 
		 *
		 */
		__populate: function(win, row) {
			var m = win.getModel();

			m.setId(row.id);
			m.setTitle(row.title);
			m.setDescription(row.description);
		},

		/**
		 * Get Table Model
		 */
		getTableModel: function() {
			var tm = ic.service.AdminModelFactory.getModel(this.__type);
			return tm;
		},

		/**
		 * Get Toolbar (overridden)
		 *
		 * 
		 *
		 */
		getToolbar: function() {

			// toolbar
			//
			var mainPart   = new qx.ui.toolbar.Part;
			var refreshButton = new qx.ui.toolbar.Button("Refresh", "ic/arrow_refresh.png");
			mainPart.add(refreshButton);

			refreshButton.addListener("execute", function(e) {
				this.getTable().getTableModel().reloadData();
			}, this);


			var addButton = new qx.ui.toolbar.Button("Add New", "ic/add.png");
			mainPart.add(addButton);

			addButton.addListener("execute", function(e) {
				var win = new ic.app.page.window.Editor();
				win.open();

				win.addListener("recordUpdate", function(e) {
					this.getTable().getTableModel().reloadData();
				}, this);

			}, this);

			var tb = new qx.ui.menubar.MenuBar();
			tb.add(mainPart);

			return tb;
		}

	}

});
