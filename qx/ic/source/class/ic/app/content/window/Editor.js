/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/save.png)

************************************************************************ */

/**
 * Node Editor Window
 * 
 *
 */
qx.Class.define("ic.app.content.window.Editor", {
	extend: ic.ui.Window,

	construct: function() {
		this.base(arguments, "Content Editor", "ic/page_white_text.png");

		this.__type = "Content";

		this.setHeight(400);
		this.setWidth(500);

		// create a model
		var mskel = ic.service.AdminModelFactory.getModelSkel(this.__type);
		this.__model = qx.data.marshal.Json.createModel(mskel);

		// layout defaults
		var layout = new qx.ui.layout.Grid();
		layout.setRowFlex(2, 1);
		layout.setColumnFlex(0, 1);

		this.setLayout(layout);

		var pane = this.getChildControl("pane");
		pane.setBackgroundColor("#dddddd");

		var tb = this.__getToolbar();
		this.add(tb, { row: 0, column: 0 });

		var tabView = new qx.ui.tabview.TabView();
		tabView.setMargin(5);
		tabView.setContentPadding(0,0,0,0);

		this.add(tabView, { row: 2, column: 0 });

		var detailsPage = new qx.ui.tabview.Page("Content Details");
		this.__detailsForm(detailsPage);
		tabView.add(detailsPage);


	},

	events: {
		"loadedRecord": "qx.event.type.Event",
		"recordUpdate": "qx.event.type.Event"
	},

	members: {

		__id: null,

		__populated: null,

		__type: null,

		__editor: null,

		__toolbar: null,

		__model: null,

		__formManager: null,

		/**
		 * Get Model
		 *
		 * 
		 *
		 */
		getModel: function() {
			return this.__model;
		},

		/**
		 * Details Form
		 *
		 * 
		 *
		 */
		__detailsForm: function(detailsPage) {
			var controller = new qx.data.controller.Object(this.__model);

			var layout = new qx.ui.layout.Grid();
			layout.setColumnFlex(1, true);
			layout.setRowFlex(1, true);


			layout.setColumnWidth(3, 140);
			layout.setColumnWidth(4, 140);

			layout.setSpacingX(5);
			layout.setSpacingY(5);

			detailsPage.setLayout(layout);
			detailsPage.setPadding(10);

			this.__formManager = new qx.ui.form.validation.Manager();


			var idField = new qx.ui.form.TextField();
			idField.setRequired(true);
			this.__formManager.add(idField);

			this.addListener("loadedRecord", function() {
				idField.setEnabled(false);
			}, this);

			var idLabel = new qx.ui.basic.Label("ID:");
			idLabel.setBuddy(idField);
			controller.addTarget(idField, "value", "id", true);

			detailsPage.add(idLabel, { row: 0, column: 0 });
			detailsPage.add(idField, { row: 0, column: 1, colSpan: 2  });


			var valueField = new qx.ui.form.TextArea();
			valueField.setRequired(true);
			this.__formManager.add(valueField);

			var valueLabel = new qx.ui.basic.Label("Value:");
			valueLabel.setBuddy(valueField);
			controller.addTarget(valueField, "value", "value", true);

			detailsPage.add(valueLabel, { row: 1, column: 0 });
			detailsPage.add(valueField, { row: 1, column: 1, colSpan: 2 });

		},


		/**
		 * Get Toolbar
		 *
		 * 
		 *
		 */
		__getToolbar: function() {

			// toolbar
			//
			var mainPart   = new qx.ui.toolbar.Part;

			var saveButton = new qx.ui.toolbar.Button("Save", "ic/save.png");
			mainPart.add(saveButton);

			saveButton.addListener("execute", function() {
				if(this.__formManager.validate()) {
					var json = qx.util.Serializer.toJson(this.__model, null);

					var req = new ic.io.framework.JSONPost(this.__type, json);
					req.send();

					req.addListener("success", function(e) {
						this.fireEvent("recordUpdate");
						this.close();
					}, this);
				}
			}, this);

			var tb = new qx.ui.menubar.MenuBar();
			tb.add(mainPart);

			return tb;
		},

		/**
		 * Generage GUID
		 *
		 * 
		 *
		 * @param len {Integer} length of guid
		 */
		__generateGUID: function(len) {
			var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
			var string_length = len;
			var randomstring = '';
			for (var i=0; i<string_length; i++) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum,rnum+1);
			}
			return randomstring;
		}

	}

});
