<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * ContentController.php
 * 
 * Contains the {@link ContentController} class.
 * 
 * @package INFLUENCE 
 * @subpackage controllers
 */ 

/**
 * ContentController class
 *
 * @package INFLUENCE 
 * @subpackage controllers
 */
class ContentController extends Controller {

	private $keywords = array("Jesus"               => true,
				  "Jesus Christ"        => true,
				  "God"                 => true,
				  "Bible"               => true,
				  "Anaheim"             => true,
				  "Yorba Linda"         => true,
				  "Orange"              => true,
				  "Orange County"       => true,
				  "Influence Church"    => true,
				  "influencechurch.org" => true,
				  "Phil Hotsenpiller"   => true);


	/**
	* Pre Action
	*
	* preAction is called by the Controller before
	* dispatching to an action.
	*
	* @access public
	*/
	public function preAction() {

		$sec = $this->configuration->environment["APP"]["cache"]["sec"];

		if($sec > 0) {
			header("HTTP/1.1 200 OK");
			header("Cache-Control: public, max-age=" . $sec);
			header("Edge-Control: max-age=" . $sec . ", !no-store, !bypass-cache");
			header("Expires: " . gmdate("D, d M Y H:i:s", time() + $sec) . " GMT");
		}

		$this->addMetaData("webServer", $this->configuration->environment["FRAMEWORK"]["web"]["server"]);
		$this->addMetaData("mediaServer", $this->configuration->environment["INFLUENCE"]["media"]["server"]);
		$this->addMetaData("googleAnalytics", $this->configuration->environment["INFLUENCE"]["google"]["analytics"]);
		$this->addMetaData("facebookAppId", $this->configuration->environment["API"]["facebook"]["appId"]);
		$this->addMetaData("google", $this->configuration->environment["API"]["google"]["key"]);
		$this->addMetaData("ogDescription", "Influence Church, Anaheim Hills, CA. To influence the world and spread God's fame, that many may believe that He is the Christ, the Son of the Living God.");
		$this->addMetaData("ogTitle", "Influence Church");
		$this->addMetaData("ogImage", "http://" . $this->configuration->environment["FRAMEWORK"]["web"]["server"] . "/images/i_315x315.png");


		parent::preAction();
	}

	/**
	* Home
	*
	* Configured as default action
	*
	*/
	public function web_home(PayloadPkg $section,
							 PayloadPkg $page,
							 PayloadPkg $ref) {

		$this->web_site($section, $page, $ref);
	}

	/**
	* Site
	*
	*
	*/
	public function web_site(PayloadPkg $section, 
							 PayloadPkg $page,
							 PayloadPkg $ref) {

		$this->addMetaData("section", $section->getString());
		$this->addMetaData("page", $page->getString());

		$view = $page->getString() ? $page->getString() : $section->getString("home");

		// page content
		//
		$mPageContent = $this->modelFactory->load("VwPageContent","INFLUENCE");
		$dataPageContent = $mPageContent->smartSelect(array("content_id","content_value"), array("page_id" => $view));

		$dataObject = new stdClass();

		if(is_array($dataPageContent)) {
			foreach($dataPageContent as $kv) {
				$dataObject->content->$kv["content_id"] = trim($kv["content_value"]);
			}
		}

		// config
		//
		$mConfig    = $this->modelFactory->load("Config","INFLUENCE");
		$configData = $mConfig->smartSelectAll();
		$config     = array();

		foreach($configData as $cfg) {
			$config[$cfg["type"]][$cfg["page"]]            = $cfg["value"];
			$config["pageCfg"][$cfg["page"]][$cfg["type"]] = $cfg["value"];
		}

		$dataObject->appConfig = $this->configuration->environment["APP"];
		$dataObject->appConfig = array_merge($config);

		// view config
		$dataObject->viewConfig = $this->configuration->environment["VIEW"][$view];

		// ref content
		//
		if($ref->getString()) {
			$dataObject->ref->id = $ref->getString();

			$refPlugin = $this->pluginFactory->load("RefData", "REF");

			if($view == "post") {
			  //$this->logger->debug(print_r($dataObject->appConfig["feed"],1));
				$feed = $dataObject->appConfig["feed"][$section->getString()];

				$dataObject->ref->content = $refPlugin->getContent($feed, $ref->getString());
			} else {
				$dataObject->ref->content = $refPlugin->getData($view, $ref->getString());
			}
		}

		$api = $this->pluginFactory->load("Api", "TUMBLR");


		// blocks
		//
		if($blocksFeed = $dataObject->appConfig["blocks"][$view]) {
			$dataObject->blocks = $api->getBlocks($blocksFeed);
		} else {
			$dataObject->blocks = null;
		}

		// carousel
		//
		if($carouselFeed = $dataObject->appConfig["carousel"][$view]) {
			$dataObject->carousel = $api->getCarousel($carouselFeed);
		} else {
			$dataObject->carousel = null;
		}

		// vimeo
		//
		if ($videoFeed = $dataObject->appConfig["pageCfg"][$view]["video"]) {
			$vapi = $this->pluginFactory->load("Api", "VIMEO");
			$dataObject->vimeo = $vapi->getChannelVideos($videoFeed);
		}

		// watch vimeo
		//
		if ($view == "watch") {
			$api = $this->pluginFactory->load("Api", "VIMEO");
			$dataObject->vimeoVideo = $api->getVideo($ref->getString());
		}

		// news
		//
		if ($newsFeed = $dataObject->appConfig["pageCfg"][$view]["feed"]) {
			if($view == "home") {
				$dataObject->article = $api->getArticle($newsFeed);
			} else {
				$dataObject->feed = $api->getPosts($newsFeed);
			}
		}

		// ministry and video blocks for home
		//
		if($view == "home") {
			if($blocksFeed = $dataObject->appConfig["blocks"]["ministries"]) {
				$dataObject->ministryBlocks = $api->getBlocks($blocksFeed);
			} else {
				$dataObject->blocks = null;
			}

			// vimeo message
			//
			if ($videoFeed = $dataObject->appConfig["pageCfg"]["message"]["video"]) {
				$vapi = $this->pluginFactory->load("Api", "VIMEO");
				$vimeo = $vapi->getChannelVideos($videoFeed);
				$dataObject->message = $vimeo->video[0];
			}

			// vimeo worship
			//
			if ($videoFeed = $dataObject->appConfig["pageCfg"]["worship"]["video"]) {
				$vapi = $this->pluginFactory->load("Api", "VIMEO");
				$vimeo = $vapi->getChannelVideos($videoFeed);
				$dataObject->worship = $vimeo->video[0];
			}

		}


		$this->addMetadata("keywords", implode(",", array_keys($this->keywords)));

		$this->setData($dataObject);

		$this->render("content/" . $view);
	}

	/**
	* Donate
	*
	*/
	public function web_donate(PayloadPkg $section, 
							   PayloadPkg $page,
							   PayloadPkg $ref,
							   DonationBundle $donation,
							   RecapBundle $recap) {

		foreach($donation->props as $prop => $propV) {
			$this->addMetaData($prop, $propV);
		}

		$challenge = $recap->check();
		if(!$challenge->is_valid) {
			$this->addMetaData("recapFail",  true);

		} else {
			$this->addMetaData("recapFail",  false);
			$api = $this->pluginFactory->load("Api", "ETAP");

			if($api->processDonation($donation->props)) {
				$this->addMetaData("processed", true);
			} else {
				$this->addMetaData("processed", false);
				$this->addMetaData("apiFault", $api->errorMessage);
			}

			$api->endSession();
		}


		$this->web_site($section, $page, $ref);
	}

	/**
	* Media
	*
	*/
	public function web_media() {


		$api = $this->pluginFactory->load("Api", "VIMEO");
		$this->setData($api->getChannelVideos("influencechurch"));

		$this->render();
	}


	/**
	* Podcast
	*
	*/
	public function web_podcast() {

		$api = $this->pluginFactory->load("Api", "SCLOUD");
		$this->setData(array("track" => $api->getTracks()));


		$this->render("audio");
	}


}
