<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * INFLUENCE_PageMediaModel.php
 * 
 * Contains the {INFLUENCE_PageMediaModel} class.
 * (this file may be modified safely)
 * 
 * @version $Rev: $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires structure
 */
require_once('../app/models/INFLUENCE/generated/INFLUENCE_PageMediaStructure.php');

/**
 * INFLUENCE_PageMediaModel class
 *
 * @package INFLUENCE 
 * @subpackage models
 */
class INFLUENCE_PageMediaModel extends INFLUENCE_PageMediaStructure {
  
}
