<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * INFLUENCE_TumblrCacheModel.php
 * 
 * Contains the {INFLUENCE_TumblrCacheModel} class.
 * (this file may be modified safely)
 * 
 * @version $Rev: $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires structure
 */
require_once('../app/models/INFLUENCE/generated/INFLUENCE_TumblrCacheStructure.php');

/**
 * INFLUENCE_TumblrCacheModel class
 *
 * @package INFLUENCE 
 * @subpackage models
 */
class INFLUENCE_TumblrCacheModel extends INFLUENCE_TumblrCacheStructure {

	/**
	 * Upsert Tumblr data
	 * 
	 * @param  string $feed   feed url
	 * @param  int    $offset 
	 * @param  int    $limit  
	 * @param  string $data 
	 */
	public function upsert($feed, $offset, $limit, $data) {
		$this->db->execute("INSERT INTO " . $this->TABLE_NAME . "(`feed`, `offset`, `limit`, `data`) VALUES (" . $this->db->prepareString($feed) . "," . $this->db->prepareNumber($offset) . "," . $this->db->prepareNumber($limit) . "," . $this->db->prepareString($data) . ") ON DUPLICATE KEY UPDATE `data` = VALUES(data)");
	}
 
	/**
	 * Get Data
	 * 
	 * @param  string $feed   feed url
	 * @param  int    $offset 
	 * @param  int    $limit  
	 * @return string
	 */
	public function getData($feed, $offset, $limit) {
		return $this->smartSelectOne(array('data'), array('feed' => $feed, 'offset' => $offset, 'limit' => $limit));
	}

}
