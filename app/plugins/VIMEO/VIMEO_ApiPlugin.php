<?php
 /*

         _                      
  __   _(_)_ __ ___   ___  ___  
  \ \ / / | '_ ` _ \ / _ \/ _ \ 
   \ V /| | | | | | |  __/ (_) |
    \_/ |_|_| |_| |_|\___|\___/ 

  vimeo api utils

  cjimti@gmail.com

*/

/** 
 * VIMEO_ApiPlugin.php
 * 
 * Contains the {@link VIMEO_ApiPlugin} class.
 * 
 * @package VIMEO
 * @subpackage plugins
 */ 

require_once("vimeo.php");

/**
 * ApiPlugin class
 *
 * @package VIMEO
 * @subpackage plugins
 */
class VIMEO_ApiPlugin extends Plugin {

	/**
	* Get Video
	*
	* 
	*/
	public function getVideo($videoId) {

		$cfg = array('video_id' => $videoId);
		$resp = $this->request('vimeo.videos.getInfo', $cfg);
		//$this->logger->debug("vimeo resp:" . print_r($resp,1));

		return $resp->video[0];
	}

	/**
	* Get Channel Videos
	*
	* 
	*/
	public function getChannelVideos($channel="influencechurch", $page=1, $perPage=50) {

		$cfg = array('channel_id'       => $channel,
					 'page'             => $page,
					 'per_page'         => $perPage,
					 'summary_response' => 1,
					 'full_response'    => 1);

		$resp = $this->request('vimeo.channels.getVideos', $cfg);
		//$this->logger->debug("vimeo resp:" . print_r($resp,1));

		return $resp->videos;
	}

	/**
	* Request
	*
	* 
	*/
	protected function request($call, $opts) {

		$consumerKey       = $this->configuration->application["VIMEO"]["key"]["clientId"];
		$consumerKeySecret = $this->configuration->application["VIMEO"]["key"]["clientSecret"];
		$userId            = $this->configuration->application["VIMEO"]["userId"];

		$opts['user_id'] = $userId;

		$this->logger->debug("vimeo call__:" . print_r($call,1));

		$vimeo  = new phpVimeo($consumerKey, $consumerKeySecret);
		$videos = $vimeo->call($call, $opts);

		$this->logger->debug("vimeo resp__:" . print_r($videos,1));


		return $videos;
	}

}
