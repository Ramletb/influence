<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/
/** 
 * ICMAP_BaseMapPlugin.php
 * 
 * Contains the {@link ICMAP_BaseMapPlugin} class.
 * 
 * @package ICMAP 
 * @subpackage plugins
 */ 

/**
 * ICMAP_BaseMapPlugin class
 *
 * @package ICMAP 
 * @subpackage plugins
 */
class ICMAP_BaseMapPlugin extends Plugin {

	/**
	* Database Package
	*
	*/
	protected $dbPackage = "INFLUENCE";

	/**
	* Model
	*
	*/
	protected $model;


	/**
	* Process
	*
	* @param Object $object
	* @access public
	*/
	public function getModel() {
		return $this->modelFactory->load($this->model, $this->dbPackage);
	}

	/**
	* Process
	*
	* @param Object $object
	* @access public
	*/
	public function process($object, $remove=0) {
		$data = $this->objectToArray($object);

		$m = $this->getModel();

		if($remove > 0) {
			// remove the record
			return $m->smartDelete($data);
		}

		// does this record exist?

		$ret = $m->smartSelect(null, array("id" => $data["id"]));
		if(is_array($ret) && count($ret) > 0) {
			return $m->smartUpdate($data, array("id" => $data["id"]));
		}

		// fell through so insert
		
		return $m->smartInsert($data);
	}


	/**
	* Object to Array
	*
	* @param Object $object
	* @return array
	* @access protected
	*/
	protected function objectToArray($object) {
		$array=array();
		foreach($object as $member=>$data) {
			$array[$member]=$data;
		}
		return $array;
	}

	/**
	* Generate UID
	*
	* @return string
	* @access protected
	*/
	protected function genUID() {
		$c1 = "0123456789abcdefghijklmnopqrstuvwxyz";
		$c2 = "----0123456789abcdefghijklmnopqrstuvwxyz";

		$string = "";
		for ($p = 0; $p < 1; $p++) {
			$string .= $c1[mt_rand(0, strlen($c1))];
		}
		for ($p = 0; $p < 6; $p++) {
			$string .= $c2[mt_rand(0, strlen($c2))];
		}
		for ($p = 0; $p < 1; $p++) {
			$string .= $c1[mt_rand(0, strlen($c1))];
		}

		return $string;
	}

}
