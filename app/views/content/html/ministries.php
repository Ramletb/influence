<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * ministries.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Ministries</h1>
	<br />
	<?php echo $this->nl2p($data->content->ministries_blurb); ?>
	<br />
</header>

<?php 

if($data->blocks) {
	require_once("widget/blocks.php"); 
} else {
	echo "<b>We're sorry. Service is temporarily unavailable.</b><br /> We are working quickly to resolve the issue. Learn more <a href=\"http://www.influencechurch.org/content/site/section/about\">about Influence Church</a>, or enjoy a <a href=\"http://www.influencechurch.org/content/site/section/message\">message online</a>.";
}

?>


<?php require_once("footer.php"); ?>