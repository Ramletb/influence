<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

$contentTitle       = "iGroups";
$contentDescription = $this->nl2p($data->content->groups_description);
$contentAbout       = $this->nl2p($data->content->groups_about);

$ogTitle       = "Influence Church - Groups";
$ogImage       = "http://24.media.tumblr.com/tumblr_mb1wn4xhe51rhiijlo1_1280.jpg";
$ogDescription = $data->content->groups_description;

require_once('header.php');

/**
 * groups.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "groups";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>