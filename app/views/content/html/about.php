<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * about.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>About Us</h1>
	<br />
	<div class="hero-unit">
		<p><?php echo $data->content->vision; ?></p>
	</div>

	<div class="subnav">
		<ul class="nav nav-pills">
			<li><a href="#StatementOfFaith">Statement Of Faith</a></li>
			<li><a href="#God">God</a></li>
			<li><a href="#JesusChrist">Jesus Christ</a></li>
			<li><a href="#HolySpirit">Holy Spirit</a></li>
			<li><a href="#Bible">Bible</a></li>
			<li><a href="#Salvation">Salvation</a></li>
			<li><a href="#Church">Church</a></li>
		</ul>
	</div>

</header>


<section id="StatementOfFaith">
	<div class="page-header">
	<h1>Statement Of Faith</h1>
	</div>
	<div class="row">
		<div class="span12">
		<p>In essential beliefs – <strong>we have unity</strong></p>
		<small>Ephesians 4:4-6</small>
		</p>

		<p>In non-essential beliefs – <strong>we have liberty</strong></p>
		<small>Romans 14:1,4,12,22</small>
		</p>

		<p>In all our beliefs – <strong>we show charity</strong></p>
		<small>1 Corinthians 13:13</small>
		</div>
	</div>
</section>

<section id="God">
	<div class="page-header">
	<h1>About God</h1>
	</div>
	<div class="row">
		<div class="span12">
		<p>There is only one living God who exists in three distinct persons: Father, Son and Holy Spirit. God is not only knowable, but also personal. He is not only our creator, but our sustainer.  The phrase Trinity has come to identify the relationship between the Father, The Son, and The Holy Spirit, to express their individual uniqueness, while affirming equality. The Bible makes it clear, the Father is God, the Son is God and the Holy Spirit is God....</p>
		<cite class="bibleref" style="font-style: normal;" title="Genesis 1">Genesis 1</cite>; Deuteronomy 6:4; <cite class="bibleref" style="font-style: normal;" title="Genesis 1-2">Genesis 1-2</cite>; <cite class="bibleref" style="font-style: normal;" title="John 17">John 17</cite>, Luke 3:21-22, Matthew 28:19-20, <cite class="bibleref" style="font-style: normal;" title="Luke 15">Luke 15</cite></a>; Romans 8:15; Matthew 6:9-15
		</div>
	</div>
</section>

<section id="JesusChrist">
	<div class="page-header">
	<h1>About Jesus Christ</h1>
	</div>
	<div class="row">
		<div class="span12">
		<p>Jesus Christ is the Son of God. He is co-equal with the Father and the Holy Spirit. Jesus was born of a virgin, lived a sinless life, died on the cross and three days later rose from the dead. By doing this, Jesus demonstrated His power over sin and death and His validity as the Son of God. He will one day return to fully bring His Kingdom to this Earth.</p>
		<small>Matthew 1:22, 23, Isaiah 9:6, John 1:1-5; 3:16, 14:10-30; Hebrews 4:14-15; <cite class="bibleref" style="font-style: normal;" title="1 Corinthians 15">1 Corinthians 15</cite>, Romans 1:3-5, Acts 1:9-11, 1 Timothy 6:14-15</small>
		</div>
	</div>
</section>

<section id="HolySpirit">
	<div class="page-header">
	<h1>About the Holy Spirit</h1>
	</div>
	<div class="row">
		<div class="span12">
		<p>The Holy Spirit is the Spirit of God. He convicts the world of sin, draws people toward Christ, cultivates godly character, gives spiritual gifts and provides needed comfort in all season of life. As Christians, we seek to live under His control daily.</p>
		<small>John 14:15-21; Acts 1:8; Galatians 5:22-23; 1 Corinthians 12:1-11; 2 Corinthians 3:17; Ephesians 1:13</small>
		</div>
	</div>
</section>

<section id="Bible">
	<div class="page-header">
	<h1>About the Bible</h1>
	</div>
	<div class="row">
		<div class="span12">
		<p>The Old and New Testament are inspired by God and is God’s revelation of Himself to man. Divine inspiration extends equally and fully to all 66 books of the Bible. The Scriptures are without error and completely trustworthy. It is the supreme standard for conduct and reveals the way of salvation through Jesus Christ our Lord. Scripture is used to teach us, direct us, and empower us to understand who God is and what it means to have a relationship with Him.</p>
		<small>Deuteronomy 4:1-2; Psalm 19:7-10; John 5:39; Romans 15:4; 2 Timothy 3:15-17; Hebrews 1:1-2, 4:12; 2 Peter 1:19-21</small>
		</div>
	</div>
</section>

<section id="Salvation">
	<div class="page-header">
	<h1>About Salvation</h1>
	</div>
	<div class="row">
		<div class="span12">
		<p>Man is separated from a relationship with God from birth because of sin. As a result, there is nothing man can do to earn salvation. Only by trusting in what Jesus Christ has done on the cross can man enter into a relationship with God. Salvation is not based on what man can do to get to God, but rather what God has done to get to man. Salvation is by grace and grace alone.</p>
		<small>Romans 3:22-24, 6:23-24; <cite class="bibleref" style="font-style: normal;" title="Romans 10">Romans 10</cite>; Ephesians 2:8-9; John 3:16; Titus 3:5; Galatians 3:26</small>
		</div>
	</div>
</section>

<section id="Church">
	<div class="page-header">
	<h1>About the Church</h1>
	</div>
	<div class="row">
		<div class="span12">
		<p>The Church is the body of Christ in the world. It is not a building or a denomination, but rather a group of called out people who have put their faith in Jesus Christ. The church exists for the primary purpose of bringing glory to God.</p>
		<small><cite class="bibleref" style="font-style: normal;" title="Ephesians 5">Ephesians 5</cite>; <cite class="bibleref" style="font-style: normal;" title="Romans 8">Romans 8</cite>; 1 Corinthians 12:13; 1 Peter 2:5; 2 Corinthians 11:2</small>
		</div>
	</div>
</section>
<br />
</div>

<br />

<section id="end">
	<img src="/images/assets/outdoors.jpg" style="width: 100%" />
</section>


<div class="container">

<?php require_once("footer.php"); ?>