<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * content-header.php
 *
 * DEFINE:
 *
 * $contentTitle
 * $contentDescription
 *
 * @package INFLUENCE
 * @subpackage views
 */

?>

<header class="jumbotron">
	<h1><?php echo $contentTitle; ?></h1>
	<br />
</header>

<div class="row">
	<div class="span12">
		<?php echo $contentDescription; ?>
	</div>
</div>
<br />

