<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Community";
$contentDescription = $this->nl2p($data->content->community_description);
$contentAbout       = $this->nl2p($data->content->community_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["community"];

$ogTitle       = "Community - Influence Church";
$ogImage       = "http://24.media.tumblr.com/tumblr_mb1w90tvO31rhiijlo1_1280.jpg";
$ogDescription = $data->content->community_description;

require_once('header.php');

/**
 * community.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "community";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>