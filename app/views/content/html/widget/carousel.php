<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * carousel.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
	<div class="carousel-inner">

<?php 
$i = 0;
foreach($data->carousel as $slide) { 

?>
		<div class="item <?php if($i == 0) { echo "active"; } ?>">
			<a href="<?php echo $slide["url"]; ?>"><img src="<?php echo $slide["photo"]; ?>" /></a>
			<div class="carousel-caption understate">
				<h4><?php echo $slide["title"]; ?></h4>
				<?php echo $this->nl2p($slide["blurb"]) . "\n"; ?>
			</div>
		</div>
<?php 
$i++;
} 
?>

	</div>

	<!-- Carousel nav -->

	<a class="carousel-control left" href="#homeCarousel" data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#homeCarousel" data-slide="next">&rsaquo;</a>

</div>
