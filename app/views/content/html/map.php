<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * about.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Map</h1>
</header>

<div class="row">
	<div class="span12">
		<h2><?php echo $data->content->service_times; ?></h2>
		<div id="service_map_canvas" style="width: 1280px; height: 710px"></div>
	</div>
</div>
</section>



<script>
function initialize() {
	var image = '/images/i_map.png';
	var imageKids = '/images/i_map_kids.png';
	var imageService2 = '/images/i_map_service2.png';
	
	var mapStyles = [
		{
			featureType: "all",
			elementType: "labels",
			"stylers": [
				{ visibility: "off" }
			]
		},
		{
			featureType: "road",
			"stylers": [
				{ "visibility": "on" },
				{ "saturation": -100 }
			]
		},
		{
			featureType: "road.local",
			elementType: "labels",
			"stylers": [
				{ visibility: "off" }
			]
		}
	];

	var styledMap = new google.maps.StyledMapType(mapStyles, { name: "Styled Map" });

	var mapOptions = {
		zoom: 15,
		center: new google.maps.LatLng(33.865225, -117.752001),
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		},
		disableDefaultUI: true
	};

	var map = new google.maps.Map(document.getElementById('service_map_canvas'), mapOptions);
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');


	mapOptions.center = new google.maps.LatLng(33.86169,-117.792208);


}

function loadScript() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "http://maps.googleapis.com/maps/api/js?key=<?php echo $google; ?>&sensor=false&callback=initialize";
	document.body.appendChild(script);
}

window.onload = loadScript;
</script>

<?php require_once("footer.php"); ?>