<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

$pageTitle = $data->vimeoVideo->title;
$ogTitle   = $pageTitle;
$ogDescription = $data->vimeoVideo->description;
$ogImage       = $data->vimeoVideo->thumbnails->thumbnail[3]->_content;

require_once('header.php');

/**
 * message.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<div id="fb-root"></div>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $facebookAppId; ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<header class="jumbotron">
	<h1><?php echo $data->vimeoVideo->title; ?></h1>
	<?php
		$desc = explode("\n", $data->vimeoVideo->description);

		echo "<h4>" . $desc[0] . "<br />";
		echo $desc[1] . "</h4>\n";
		echo "<h6>" . $desc[2] . "</h6>\n";
		echo "<p>" . $desc[3] . "</p>\n";
	?>
</header>

<div class="row">
	<div class="span12">
		<iframe src="http://player.vimeo.com/video/<?php echo $data->vimeoVideo->id; ?>?title=0&amp;portrait=0&amp;byline=0" style="width: 100%; height: 450px" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>


<div class="tab-content">

		<ul class="nav nav-tabs" id="groupsTab">
			<li class="active"><a href="#comment" data-toggle="tab">Comment</a></li>
			<!-- <li><a href="#bible" data-toggle="tab">Bible</a></li> -->
			<!-- <li><a href="#outline" data-toggle="tab">Outline</a></li> -->
		</ul>

		<div class="tab-pane active" id="comment">
			<p>
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style ">
				<a class="addthis_button_preferred_1"></a>
				<a class="addthis_button_preferred_2"></a>
				<a class="addthis_button_preferred_3"></a>
				<a class="addthis_button_preferred_4"></a>
				<a class="addthis_button_compact"></a>
				<a class="addthis_counter addthis_bubble_style"></a>
				</div>
				<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
				<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-501b8a153e544944"></script>	<!-- AddThis Button END -->
			</p>

		</div>

<!--
		<div class="tab-pane" id="bible">
			<script type="text/javascript" src="http://ebible.com/assets/ministry/ebible.embed.js?corporate_id=822"></script> <a href="http://ebible.com" style="margin: 8px 0;display:block;">Online Bible by eBible.com</a>
		</div>

		<div class="tab-pane active" id="outline">

			<iframe height="600px" width="100%" frameborder="0"  src="https://docs.google.com/a/influencechurch.org/document/d/1B6CH_4tqUBEowswWpxGRiZ_SgUopXtjDCddEsg_SQHo/edit?pli=1"></iframe>
		</div>
-->

</div>


	</div>

</div>

<?php require_once("footer.php"); ?>
