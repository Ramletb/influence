<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Women of Influence";
$contentDescription = $this->nl2p($data->content->women_description);
$contentAbout       = $this->nl2p($data->content->women_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["women"];

$ogTitle       = "Influence Church - Women";
$ogImage       = "http://24.media.tumblr.com/tumblr_mb1wuiTzgx1rhiijlo1_r1_500.jpg";
$ogDescription = $data->content->women_description;

require_once('header.php');

/**
 * women.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "women";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>