<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

$contentTitle       = "Events";
$contentDescription = $this->nl2p($data->content->events_description);
//$contentAbout       = $this->nl2p($data->content->events_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["events"];

$ogTitle       = "Events - Influence Church";
$ogImage       = "http://25.media.tumblr.com/tumblr_mb1wjjsfYQ1rhiijlo1_1280.jpg";
$ogDescription = $data->content->events_description;

require_once('header.php');

/**
 * events.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "events";

?>
<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>