<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');
require_once('parser/TumblrParser.php');

/**
 * blog.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<style>
	h6.newsDate {
		color: #999; 
		text-transform: uppercase; 
		padding-top: 0;
	}

	h2.newsTitle {
		line-height: 30px;
	}

</style>

<header class="jumbotron">
	<h1>News</h1>
	<br />
</header>

<div class="row">
	<div class="span12" id="blog">
<?php
$tp = new TumblrParser();

if($data->feed && $data->feed->posts) {
	foreach($data->feed->posts as $post) {
		$article = $tp->parsePost($post);

		echo '<div class="row">';
		echo '<div class="span1"><h6 class="newsDate">' . date('M j Y', $article["timestamp"]) . '</h6></div>';
		echo '<div class="span11"><a href="/news/' . $article["id"] . '/' . $post->slug . '"><h2 class="newsTitle">' . $article["title"] . '</h2></a>';
		echo $article["body"];
		echo '</div>';
		echo '</div>';
	}
} else {
	echo "<b>We're sorry. Service is temporarily unavailable.</b><br /> We are working quickly to resolve the issue. Learn more <a href=\"http://www.influencechurch.org/content/site/section/about\">about Influence Church</a>, or enjoy a <a href=\"http://www.influencechurch.org/content/site/section/message\">message online</a>.";
}
?>
	</div>
</div>


<?php require_once("footer.php"); ?>
