<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * message.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Message</h1>
	<br />
	<?php echo $this->nl2p($data->content->message_blurb); ?>
	<?php echo $this->nl2p($data->content->message_podcast); ?>
	<br />
</header>

<div class="row">
	<div class="span12">

<?php if($data->vimeo->video) { ?>
<table class="table table-striped table-bordered ">
	<thead>
		<tr>
			<th>Title</th>
			<th>Video</th>
		</tr>  
	</thead>
<?php foreach($data->vimeo->video as $video)  {  if($video->privacy == "anybody")  { ?>
<tr>
		<td>
			<h4><?php echo $video->title; ?></h4>
<?php
	$desc = explode("\n", $video->description);

	echo "<h5>" . $desc[0] . "<br />";
	echo $desc[1] . "</h5>\n";
	echo "<h6>" . $desc[2] . "</h6>\n";
	echo "<p>" . $desc[3] . "</p>\n";
?>
		</td>

		<td><div style="background-image: url(<?php echo $video->thumbnails->thumbnail[2]->_content; ?>); background-repeat: no-repeat; background-position: center top; background-size: 100%;"><a href="/message/<?php echo $video->id; ?>/<?php echo preg_replace("/\s+/", "+", ereg_replace("[^A-Za-z0-9]", " ", $video->title)); ?>"><center><img src="/images/play-button-overlay-8.png" alt="<?php echo $video->title; ?>" /></center></a></div></td>


</tr>

<?php } } ?>
</table>

<?php } ?>
<p>
<a href="https://influencechurch.org/sermons"><button class="btn btn-primary">Browse All Sermons on Vimeo</button></a>
</p>
</div>
</div>

<?php require_once("footer.php"); ?>