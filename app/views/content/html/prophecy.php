<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Prophecy Conference";
//$contentDescription = $this->nl2p($data->content->moms_description);
//$contentAbout       = $this->nl2p($data->content->moms_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
//$contentFeed      = $data->appConfig["feed"]["prophecy"];

$ogTitle       = "Prophecy Conference - Influence Church";
$ogImage       = "http://24.media.tumblr.com/tumblr_mb1tbsoYdT1rckv38o1_r2_1280.jpg";
//$ogDescription = $data->content->moms_description;

require_once('header.php');

/**
 * prophecy.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "prophecy";
?>

<?php require_once("content-header.php"); ?>

<?php require_once("content-tabs.php"); ?>

<?php require_once("contact-form.php"); ?>

<?php require_once("footer.php"); ?>