<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Young Adults";
$contentDescription = $this->nl2p($data->content->college_description);
//$contentAbout       = $this->nl2p($data->content->college_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["college"];

$ogTitle       = "Young Adults - Influence Church";
$ogImage       = "http://24.media.tumblr.com/tumblr_mb1w7bpDKA1rhiijlo1_1280.jpg";
$ogDescription = $data->content->college_description;

$navTitle = "Young Adults";

require_once('header.php');

/**
 * youngadults.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$contentCalendar = false;
$page = "youngadults";
$data->appConfig["pageCfg"][$page]["gcal"] = '';

?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>