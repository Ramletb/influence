<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * form.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Form</h1>
	<br />
</header>

<div class="row">
	<div class="span12" id="form">
		<iframe src="https://docs.google.com/a/influencechurch.org/spreadsheet/embeddedform?formkey=dHptazhsVm51Q3J0YmFYMTdzR3ZFaWc6MQ" width="760" height="952" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
	</div>
</div>

<?php require_once("footer.php"); ?>
