<?php
 /*

  _         _               _      
 | |_ _   _| |__  _ __ ___ | |_ __ 
 | __| | | | '_ \| '_ ` _ \| | '__|
 | |_| |_| | |_) | | | | | | | |   
  \__|\__,_|_.__/|_| |_| |_|_|_|   

 tumblr view parser

 cjimti@gmail.com

*/

/** 
 * tumblr.php
 * 
 * Contains the {@link TUMBLR_ApiPlugin} class.
 * 
 * @package TUMBLR 
 * @subpackage utils
 */ 

/**
 * TumblrParser class
 *
 * @package TUMBLR 
 * @subpackage utils
 */
class TumblrParser {

	/**
	* Parse Post
	*
	* 
	*/
	public function parsePost($post) {
		$article = array("id"        => $post->id,
						 "slug"      => $post->slug,
						 "timestamp" => $post->timestamp,
						 "type"      => $post->type,
						 "title"     => "");

		switch ($post->type) {

			case "text":
				$article = $this->parseTextPost($post, $article);
			break;

			case "photo":
				$article = $this->parsePhotoPost($post, $article);
			break;

			case "quote":
				$article = $this->parseQuotePost($post, $article);
			break;

			case "link":
				$article = $this->parseLinkPost($post, $article);
			break;

			case "chat":
				$article = $this->parseChatPost($post, $article);
			break;

			case "audio":
				$article = $this->parseAudioPost($post, $article);
			break;

			case "video":
				$article = $this->parseVideoPost($post, $article);
			break;

			default:

		}

		return $article;
	}

	/**
	* Parse Photo Post
	*
	* 
	*/
	protected function parsePhotoPost($post, $article) {
		$a = "";

		if($post->link_url && $post->link_url != "") {
			$a = '<a href="' . $post->link_url . '">';
		}
		$img = '<p>' . $a . '<img src="' . $post->photos[0]->original_size->url . '" /></a></p>';

		if($a != "") {
			$img .= "</a>";
		}

		$article["body"] = $img . "<p>" . $post->caption . "</p>";

		return $article;
	}

	/**
	* Parse Text Post
	*
	* 
	*/
	protected function parseTextPost($post, $article) {
		$article["title"] = $post->title;
		$article["body"]  = $post->body;

		return $article;
	}

	/**
	* Parse Quote Post
	*
	* 
	*/
	protected function parseQuotePost($post, $article) {
		$article["body"] = '<div class="hero-unit"><p>' . $post->text . '<p><p><small>' . $post->source . '</small></p></div>';
		return $article;
	}

	/**
	* Parse Link Post
	*
	* 
	*/
	protected function parseLinkPost($post, $article) {
		$article["body"] = '<div class="hero-unit"><p><a href="' . $post->url . '">' . $post->title . '</a></p></div>';
		return $article;
	}

	/**
	* Parse Chat Post
	*
	* 
	*/
	protected function parseChatPost($post, $article) {
		$article["title"] = $post->title;
		$article["body"] = '<dl class="dl-horizontal">';

		foreach($post->dialogue as $d) {
			$article["body"] .= '<dt>' . $d->name . '</dt>';
			$article["body"] .= '<dd>' . $d->phrase . '</dd>';
		}

		$article["body"] .= "</dl>";

		return $article;
	}

	/**
	* Parse Chat Post
	*
	* 
	*/
	protected function parseAudioPost($post, $article) {
		$article["body"] = '<p>' . $post->player . '</p><p>' . $post->caption . '</p>';
		return $article;
	}

	/**
	* Parse Video Post
	*
	* 
	*/
	protected function parseVideoPost($post, $article) {
		$article["body"]  = '<p>' . $post->player[1]->embed_code . '</p>';
		$article["body"] .= $post->caption;
		return $article;
	}

	/**
	* Get Key
	*
	* 
	*/
	protected function getKey() {
		return "&api_key=" . $this->configuration->environment["API"]["tumblr"]["key"];
	}

}
