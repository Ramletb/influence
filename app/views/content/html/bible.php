<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * bible.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Bible</h1>
	<br />
</header>

<div class="row">
	<div class="span2">
		<p><a href="http://ebible.com" style="margin: 8px 0;display:block;">Online Bible by eBible.com</a></p>
	</div>

	<div class="span10">
		<p><script type="text/javascript" src="http://ebible.com/assets/ministry/ebible.embed.js?focus_verse=exodus3&corporate_id=822"></script></p>
	</div>
</div>

<?php require_once("footer.php"); ?>
