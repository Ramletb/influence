<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * home.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<div id="homeCarousel" class="carousel slide understate">

<?php 
$day  = date("l"); 
$time = (int) date("Gi");
date_default_timezone_set('America/Los_Angeles');

if($day == "Sunday" && $time > 900 && $time < 1101) {
		require_once("widget/livestream.php"); 
} else {
	if($data->carousel) {
		require_once("widget/carousel.php"); 
	}
}
?>

<div class="row understate">
	<div class="span4">
		<h2>About Us</h2>
		<?php echo $this->nl2p($data->content->about_blurb); ?>
		<p><a href="/about"><span class="label label-info">Read More &raquo;</span></a></p>

		<h2>Influence Church</h2>
		<b><?php echo $this->nl2p($data->content->service_times); ?></b>
		<?php echo $this->nl2p($data->content->service_address); ?>

		<p><a class="btn btn-mini" href="/location"><i class="icon-map-marker"></i> Map & Directions</a></p>

<?php if(count($data->ministryBlocks) > 0 ) { ?>

		<h2>Ministries</h2>
		<?php if ($ministryBlocks = $data->ministryBlocks) { ?>
		<div>
			<?php foreach ($ministryBlocks as $ministryBlock) { ?>
				<div style="position: relative; float: left; background-size:140px 80px; height: 80px; width: 140px; margin-right:5px; margin-bottom:5px; background-image:url('<?php echo $ministryBlock["photo"]; ?>')">
				<span style="background-color:rgba(0,0,0,0.5); position: absolute; bottom: 0; right: 0; color: white; padding: 3px; width: 132px; padding-left:5px">
				<a style="color: white;" href="<?php echo $ministryBlock["url"]; ?>"><?php echo $ministryBlock["title"]; ?></a>
				</span>
				</div>
			<?php } ?>
		</div>
		<?php } ?>

		<div style="clear:both;" />
		<?php // echo $this->nl2p($data->content->ministries_blurb); ?>
		<p><a href="/ministries"><span class="label label-info">Read More &raquo;</span></a></p>
		</div>

<?php } ?>

	</div>
	<div class="span8">

		<?php if ($data->article && $data->article["timestamp"] > 0) { 
			$article = $data->article;
		?>

		<h2>News</h2>
		<a href="/news/<?php echo $article["id"]; ?>/<?php echo $article["slug"]; ?>"><p><h6><?php echo date('l jS \of F Y', $article["timestamp"]); ?></h6></p></a>
		<div id="news_block">
			<a href="/news/<?php echo $article["id"]; ?>/<?php echo $article["slug"]; ?>"><h3><?php echo $article["title"]; ?></h3></a>
			<?php echo $article["body"]; ?>
		</div>

		<p><a href="/news"><span class="label label-info">More News &raquo;</span></a></p>
<br />

		<?php } ?>

		<h2>Message</h2>

		<?php $video = $data->message; ?>

		<h4><a href="/message/<?php echo $video->id; ?>/<?php echo preg_replace("/\s+/", "+", ereg_replace("[^A-Za-z0-9]", " ", $video->title)); ?>"><?php echo $video->title; ?></a></h4>
		<?php
			$desc = explode("\n", $video->description);

			echo "<h5>" . $desc[0] . "<br />";
			echo $desc[1] . "</h5>\n";
			echo "<h6>" . $desc[2] . "</h6>\n";
			echo "<p>" . $desc[3] . "</p>\n";
		?>

		<div style="background-image: url(<?php echo $video->thumbnails->thumbnail[2]->_content; ?>); background-repeat: no-repeat; background-position: center top; background-size: 100%;"><a href="/message/<?php echo $video->id; ?>/<?php echo preg_replace("/\s+/", "+", ereg_replace("[^A-Za-z0-9]", " ", $video->title)); ?>"><center><img src="/images/play-button-overlay-8.png" alt="<?php echo $video->title; ?>" /></center></a></div>
<br />
		<p><a href="/message"><span class="label label-info">More Messages &raquo;</span></a></p>
<br />
				<h2>Worship</h2>

				<?php $video = $data->worship; ?>

				<h4><a href="/worship/watch/<?php echo $video->id; ?>/<?php echo preg_replace("/\s+/", "+", ereg_replace("[^A-Za-z0-9]", " ", $video->title)); ?>"><?php echo $video->title; ?></a></h4>
				<?php
					$desc = explode("\n", $video->description);

					echo "<h5>" . $desc[0] . "<br />";
					echo $desc[1] . "</h5>\n";
					echo "<h6>" . $desc[2] . "</h6>\n";
					echo "<p>" . $desc[3] . "</p>\n";
				?>

				<div style="background-image: url(<?php echo $video->thumbnails->thumbnail[2]->_content; ?>); background-repeat: no-repeat; background-position: center top; background-size: 100%;"><a href="/worship/watch/<?php echo $video->id; ?>/<?php echo preg_replace("/\s+/", "+", ereg_replace("[^A-Za-z0-9]", " ", $video->title)); ?>"><center><img src="/images/play-button-overlay-8.png" alt="<?php echo $video->title; ?>" /></center></a></div>
		<br />
				<p><a href="/worship"><span class="label label-info">More Worship &raquo;</span></a></p>


	</div>

</div>


<?php require_once("footer.php"); ?>
