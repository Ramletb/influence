<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

$bios = array(
	"Phil Hotsenpiller"  => array("Senior Pastor", "phil.jpeg", $data->content->bio_phil),
	"Tammy Hotsenpiller" => array("Executive Director", "tammy.jpeg", $data->content->bio_tammy),
	//"Jennifer Rottiers"  => array("Executive Assistant", false, 'You can contact Jennifer via email at <a href="mailto:jennifer@influencechurch.org">jennifer@influencechurch.org</a>'),
	"Karly Wood"         => array("Finance Manager", false, $data->content->bio_karly),
	"Jon Ketchum"        => array("Worship Pastor", "JonKetchem.jpg", $data->content->bio_jon),
	"Kellen Mills"       => array("Worship Pastor", "KellenMills.jpg", $data->content->bio_kellen),
	"Nathan Roberts"     => array("Student Ministry Pastor", "NathanRoberts.jpg", $data->content->bio_nathan),
	"Emily Ketchum"      => array("Director of Children's Ministries", "EmilyKetchum.jpg", $data->content->bio_emily),
	"Rhodora Sall"       => array("Preschool Ministries", "RhodoraSall.jpg", $data->content->bio_rhodora),
	"Marlene Miracle"    => array("Coffee Shop Manager", false, $data->content->bio_marlene),
	"Jaymee Stocks"      => array("Wedding Coordinator", false, $data->content->bio_jaymee),
	"Whitney Youmans"    => array("Intern", "WhitneyYoumans.jpg", 'You can contact Whitney via email at <a href="mailto:whitney@influencechurch.org">whitney@influencechurch.org</a>'),
	"Leilani Ahia"       => array("Intern", "LeilaniAhia.jpg", 'You can contact Leilani via email at <a href="mailto:leilani@influencechurch.org">leilani@influencechurch.org</a>'),
);

/**
 * leadership.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Leadership Team</h1>
	<br />
</header>

<?php foreach($bios as $name => $bio) { ?>

<div class="row">
	<div class="span3">
		<div class="thumbnail">
			<?php if(is_array($bio) && $bio[1]) { ?><img src="/images/assets/staff/<?php echo $bio[1]; ?>" alt="<?php echo $name; ?>" /><?php } ?>
			<div class="caption">
				<h3><?php echo $name; ?></h3>
				<h5><?php if(is_array($bio) && $bio[0]) { echo $bio[0]; } ?></h5>
			</div>
		</div>
	</div>
	<div class="span9">
		<?php echo $this->nl2p($bio[2]); ?>
	</div>
</div>


<div class="row">
	<div class="span12 icListPad">
	</div>
</div>

<?php } ?>

<?php require_once("footer.php"); ?>