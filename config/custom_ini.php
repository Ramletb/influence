<?php 

/**
 * PHP Set additional paths.
 */
ini_set('include_path', 
	'../core/frameworks' .
	':../core/databases' .
	':../core/renderers' .
	':../core/lib' .
	':../core/models' .
	':../core/services' .
	':../pear/php' .
	':../app/plugins/lib');

/**
 * PHP Set display errors. 
 */
ini_set('display_errors', false);

/**
 * PHP Set error logging.
 */
ini_set('log_errors', true);

/**
 * PHP Set error log.
 */
ini_set('error_log', '../logs/phperrors.log');

error_reporting(E_ALL ^ E_NOTICE);