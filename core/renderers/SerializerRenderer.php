<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * SerializerRenderer.php
 *
 * Contains the abstract {@link SerializerRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract SerializerRenderer Class
 *
 *
 * @package SP5
 * @subpackage core
 */
abstract class SerializerRenderer extends Renderer {

  /**
   * Type
   *
   * Type of Renderer
   *
   */  
  const TYPE = 'Serializer';


}

?>
