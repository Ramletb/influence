<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * TransformationRenderer.php
 *
 * Contains the abstract {@link TransformationRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract TransformerRenderer Class
 *
 * Renderers extending TransformerRenderer 
 * instantiate and use {@link Transformer} class objects.
 *
 * @package SP5
 * @subpackage core
 */
abstract class TransformerRenderer extends Renderer {

  /**
   * Type
   *
   * Type of Renderer
   *
   */  
  const TYPE = 'Transformer';

  /**
   * Transformer
   *
   * 
   * @param $transformer hold a {@link Transformer} class object.
   * @access protected
   */  
  protected $transformer;

  /**
   * Load Transformation View
   *
   * Loads a Trasnformation view.
   *
   * @access protected
   * @param string $view a transformation view name
   */
  protected function loadTransformation($view=null) {

    $view = $this->getViewDetails($view,TRUE);

    // Check for the view
    //
    if($view['foundFile']) {

      // requires the view
      //
      require($view['filePath']);

      // get the class name
      //
      $class = $view['name'];

      // instantiate the class
      //
      $this->transformer = new $class;

      // what abstract class should this class decend?
      //
      $ancestor = $this->prefix . 'Transformer';

      // get ancestors
      //
      $ancestors = $this->getAncestors($this->transformer);

      // does the class impement the expected interface?
      //
      if($ancestor != $ancestors[1]) {
	$this->logger->error('WARNING: [' . $class . '] MUST DIRECTLY DECEND FROM [' . $ancestor . ']');
      }

      // set data and meta data
      //
      $this->transformer->setMetaData($this->metaData);
      $this->transformer->setData($this->data);

      // set view subdirectory
      //
      $this->transformer->setViewSubdirectory($this->viewSubdirectory);


    } else {

      // no view "file" was found
      // 
      $this->logger->error('NO TRANSOFRMATION FOUND AT [' . $view['filePath'] . ']');
    }


  }


  /**
   * Get Ancestors
   *
   * Get the ancestors of a given class.
   *
   * @param string $class
   $ @returns array An array of ancestors.
   * @access private
   */  
  private function getAncestors($class) {
    
    for ($classes[] = $class; $class = get_parent_class($class); $classes[] = $class);
    return $classes;
    
  }


}

?>
