<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Renderer.php
 *
 * Contains the abstract {@link Renderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require PresentationRenderer.php
 *
 * Requires the {@link PresentationRenderer} class
 * contained in {@link PresentationRenderer.php}.
 */
require_once('PresentationRenderer.php');

/**
 * Require TransformerRenderer.php
 *
 * Requires the {@link TransformerRenderer} class
 * contained in {@link TransformerRenderer.php}.
 */
require_once('TransformerRenderer.php');

/**
 * Require SerializerRenderer.php
 *
 * Requires the {@link SerializerRenderer} class
 * contained in {@link SerializerRenderer.php}.
 */
require_once('SerializerRenderer.php');

/**
 * Require Transformer.php
 *
 * Requires the {@link Transformer} class
 * contained in {@link Transformer.php}.
 */
require_once('Transformer.php');

/**
 * The Abstract Renderer Class
 *
 *
 * @package SP5
 * @subpackage core
 */
abstract class Renderer extends App {

  /**
   * Data
   *
   * Holds data for the renderer
   *
   * @var mixed $data
   * @access protected
   */  
  protected $data;

  /**
   * Meta Data
   *
   * Holds meta data for the renderer
   *
   * @var Array $viewData
   * @access protected
   */  
  protected $metaData = array();

  /**
   * View Subdirectory
   *
   * Holds the location of a view subdirectory
   *
   * @var string $viewSubdirectory
   * @access protected
   */  
  protected $viewSubdirectory;

  /**
   * Options
   *
   * Holds a hash of optional configuration options
   *
   * @var array $optionsHash
   * @access protected
   */  
  protected $optionsHash = array();

  /**
   * Message
   *
   * Holds a message for renderers
   *
   * @var string $message
   * @access protected
   */  
  protected $message;

  /**
   * Framework
   *
   * Holds a pointer to the framework
   *
   * @var Framework
   * @access protected
   */  
  protected $framework;

  /**
   * Constructor
   * 
   * Constructs the {@link Renderer}.
   * 
   * @ignore
   * @access public
   */  
  public function __construct() {
    parent::__construct();
  }

  /**
   * Set Options
   *
   * Sets optional configuration options via hash
   *
   * @access public
   * @param array $optionsHash
   */
  public function setOptions($optionsHash) {
    $this->optionsHash = $optionsHash;
  }

  /**
   * Set Message
   *
   * Sets an additional message for some renderers,
   *
   * @access public
   * @param string $message
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * Set Framework
   *
   * Sets the framework
   *
   * @access public
   * @param Framework pointer to a {@link Framework} object.
   */
  public function setFramework($framework) {
    $this->framework = $framework;
  }

  /**
   * Set Data
   *
   * Sets data to render.
   *
   * @access public
   * @param mixed $data
   */
  public function setData($data) {
    $this->data = $data;
  }

  /**
   * Set Meta Data
   *
   * Sets meta data for the renderer.
   *
   * @access public
   * @param array $metaDataHash
   */
  public function setMetaData($metaDataHash) {
    if(is_array($metaDataHash)) {
      foreach($metaDataHash as $key => $val) {
	if(is_object($val) && is_subclass_of($val, "Core")) {
	  $this->logger->fatal("FATAL_ERROR: Attemped to pass a framework object to addMetaData. This is an UNSAFE opperation and a SECURITY RISK!");
	  exit(1);
	}
	$this->addMetaData($key, $val);
      }
    } else {
      $this->logger->trace("WARNING: setMetaData requires an array...");
    }
  }

  /**
   * Add Metat Data
   *
   * Add meta data to renderer.
   *
   * @access public
   * @param string $var variable name
   * @param mixed $val variable value
   */
  public function addMetaData($var,$val) {
    if(is_object($val) && is_subclass_of($val, "Core")) {
      $this->logger->fatal("FATAL_ERROR: Attemped to pass a framework object to addMetaData. This is an UNSAFE opperation and a SECURITY RISK!");
      exit(1);
    }

    $this->metaData[$var] = $val;
  }

  /** 
   * Set View Subdirectory
   *
   * Sets a the subdirectory to find views (templates)
   *
   * @access public 
   * @param string $subdirectory subdirectory
   */
  public function setViewSubdirectory($subdirectory) {
    $this->viewSubdirectory = $subdirectory;
  }

  /**
   * Render
   *
   * Renders data to standard out / webpage.
   *
   * @access public 
   * @param string $view an optional view parameter.
   */
  public function render($view=null) {
    $this->loadView($view);
  }

  /**
   * Render Return
   *
   * Renders data and returns it as a string.
   *
   * @access public 
   * @param string $view an optional view parameter.
   * @return string rendered output
   */
  public function renderReturn($view=null) {
    ob_start();
    $this->logger->trace("got renderReturn...");
    $this->render($view);
    return ob_get_clean();
  }

  /**
   * Content Type
   *
   * Return the content type for the rendered data.
   *
   * @access public 
   * @return string content type
   */
  public function contentType() {
    return "";
  }

  /**
   * Headers
   *
   * Returns an array of HTTP headers
   *
   * @access public 
   * @return array headers
   */
  public function headers() {
    return array();
  }

  /**
   * Get View Details
   *
   * Returns the view details.
   *<pre>
   * [name]      => DefaultIndexRssTransformer
   * [fileName]  => DefaultIndexRssTransformer.php
   * [directory] => ../app/views/default/rss
   * [filePath]  => ../app/views/default/rss/DefaultIndexRssTransformer.php
   * [foundFile] => 1
   *</pre>
   *
   * @access protected
   * @param string $view an optional view parameter.
   * @param bool $isTransformer is view a {@link Transformer} class?
   * @return array A hash of details related to the view
   */
  protected function getViewDetails($view=null,$isTransformer=false) {

    $subDir  = $this->viewSubdirectory; // name of controller
    $viewDir = strtolower($this->prefix);
    $dir     = $subDir . '/' . $viewDir;

    // if no view is specified 
    // make a view name using thedefault from configuration
    //
    if ($view == null || $view == '') {
      if($isTransformer) {
	$name  = ucfirst($subDir);
	$name .= ucfirst($this->configuration->application['FRAMEWORK']['default']['view']);
	$name .= ucfirst($viewDir);
	$name .= 'Transformer';
      } else {
	$name  = $this->configuration->application['FRAMEWORK']['default']['view'];
      }

      // since we are using a default view name
      // we need to check the standard location and 
      // the core location
      //
      $directory = C_PATH_VIEWS . $dir;
      $filePath  = $directory . '/' . $name . '.php';
      $foundFile = is_file($filePath);
 
      if(!$foundFile) {
	$this->logger->trace("View not found at [" . $filePath. "]");
	$this->logger->trace("Now checking core...");
	$directory = C_PATH_CORE_VIEWS . $dir;
	$filePath  = $directory . '/' . $name . '.php';
	$foundFile = is_file($filePath);
	if(!$foundFile) {
	  $this->logger->trace("View not found in core at [" . $filePath. "].");
	  $directory = C_PATH_VIEWS . $dir;
	  $filePath  = $directory . '/' . $name . '.php';
	} else {
	  $this->logger->trace("Found [" . $filePath. "]...");
	}
      }

    } else {

      $this->logger->trace("Render: [" . $view . "]");

      // $view was specified, so we need to process it
      //
      $bits     = explode('/', $view);
      $bitCount = count($bits);
      $result   = array();
      
      $name    = array_pop($bits);
      $vSubDir = array_pop($bits);
      $core    = array_pop($bits);
      
      if($vSubDir) {
	$subDir = $vSubDir;
      }

      if($isTransformer) {
	$name   = ucfirst($subDir) . ucfirst($name) . ucfirst($viewDir) . 'Transformer';
      }


      if($core == 'core') {
	$directory = C_PATH_CORE_VIEWS . $subDir . '/' . $viewDir;
      } else {
	$directory = C_PATH_VIEWS . $subDir . '/' . $viewDir;
      }

      // let's see if the file exists
      //
      $filePath  = $directory . '/' . $name . '.php';
      $foundFile = is_file($filePath);
      
      if(!$foundFile) {
	$this->logger->trace("WARNING: View specified but not found at [" . $filePath. "].");
      }


    }

    return array('name'       => $name,
		 'fileName'   => $name . '.php',
		 'directory'  => $directory,
		 'filePath'   => $filePath,
		 'foundFile'  => $foundFile);

  }

  /**
   * Load view
   *
   * Loads a view.
   *
   * @access protected
   * @param string $view and optional view name
   */
  protected function loadView($view=null) {
    // adds variables available to the view scope
    //    
    $configuration = $this->configuration;
    $logger        = $this->logger;
    $data          = $this->data;
    $metaData      = $this->metaData;

    if(count($this->metaData) > 0) {
      foreach($this->metaData as $var => $val) {
	$$var = $val;
      }
    }

    $viewDetails = $this->getViewDetails($view);

    // Check for the view
    //
    if($viewDetails['foundFile']) {

      // requires the view
      //
      require($viewDetails['filePath']);

    } else {

      // no view "file" was found
      // call noView
      $this->logger->error('Expected view [' . $viewDetails['name'] . '] not found.');
      $this->logger->error('No file at [' . $viewDetails['filePath'] . ']');

      $this->handleMissingView($viewDetails['name'], $viewDetails['filePath']);
    }

  }

  /**
   * Handle Missing View
   *
   * Handle a missing view condition
   * 
   * @access protected
   * @param string $view view name
   * @param string $file view file
   */
  protected function handleMissingView($view,$file) {
    $this->logger->trace("handling MissingView...");

    if($this->configuration->environment['FRAMEWORK']['development']['mode']) {

      $message  = 'The current renderer requires a view. The file [' . $file . '] was not found. ';
      $message .= 'This view and message is rendered in development mode only.';

      $dumpRenderer = $this->rendererFactory->load('Dump');
      $dumpRenderer->setMessage($message);
      $dumpRenderer->setData($this->data);
      $dumpRenderer->setMetaData($this->metaData);
      $dumpRenderer->render();
    } else {
      $this->logger->error("NO CONDITION TO HANDLE MISSING VIEW");
    }

  }

}
