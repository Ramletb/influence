<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Logger.php
 *
 * Containes Logger constants and the {@link Logger} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Trace
 *
 * C_LOGGER_TRACE level 0
 */
define('C_LOGGER_TRACE', 0);

/**
 * Debug
 *
 * C_LOGGER_DEBUG level 1
 */
define('C_LOGGER_DEBUG', 1);

/**
 * Info
 *
 * C_LOGGER_INFO level 2
 */
define('C_LOGGER_INFO',  2);

/**
 * Error
 *
 * C_LOGGER_ERROR level 3
 */
define('C_LOGGER_ERROR', 3);

/**
 * Fatal
 *
 * C_LOGGER_FATAL level 4
 */
define('C_LOGGER_FATAL', 4);

/**
 * The Logger Class
 *
 * See the {@link local:docs/r.php?t=&s=logger Logger} section of the framework manual. 
 *
 * @package SP5
 * @subpackage core
 */
class Logger {

  /**
   * Debug
   *
   * Debug. Logging level 1
   *
   * @var int $DEBUG
   * @access public
   * @static
   */  
  public static $DEBUG=1;

  /**
   * Info
   *
   * Info. Logging level 2
   *
   * @var int $INFO
   * @access public
   * @static
   */  
  public static $INFO=2;

  /**
   * Error
   *
   * Error. Logging level 3
   *
   * @var int $ERROR
   * @access public
   * @static
   */  
  public static $ERROR=3;

  /**
   * Fatal
   *
   * Fatal. Logging level 4
   *
   * @var int $FATAL
   * @access public
   * @static
   */  
  public static $FATAL=4;
  
  /**
   * Levels
   *
   * Logging levels
   *
   * @var Array $levels
   * @access public
   * @static
   */  
  public static $levels = array('TRACE', 'DEBUG', 'INFO ', 'ERROR', 'FATAL');

  /**
   * PHP Error Types
   *
   * @var Array $phpErrorType
   * @access public
   * @static
   */  
  public static $phpErrorType = array (
				       E_ERROR             => 'ERROR',
				       E_WARNING           => 'WARNING',
				       E_PARSE             => 'PARSING ERROR',
				       E_NOTICE            => 'NOTICE',
				       E_CORE_ERROR        => 'CORE ERROR',
				       E_CORE_WARNING      => 'CORE WARNING',
				       E_COMPILE_ERROR     => 'COMPILE ERROR',
				       E_COMPILE_WARNING   => 'COMPILE WARNING',
				       E_USER_ERROR        => 'USER ERROR',
				       E_USER_WARNING      => 'USER WARNING',
				       E_USER_NOTICE       => 'USER NOTICE',
				       E_STRICT            => 'STRICT NOTICE'
				       );
  
  
  /**
   * Configuration
   *
   * Framework configuration.
   *
   * @var Configuration $configuration
   * @access private
   * @static
   */  
  private static $configuration;

  /**
   * Log File Handle
   *
   * The log file handle,
   *
   * @var resource $logFileHandle
   * @access private
   * @static
   */  
  private static $logFileHandle;

  /**
   * Class Name
   *
   * The logging class,
   *
   * @var resource $className
   * @access private
   */  
  private $className;
  
  /**
   * Get Logger
   *
   * Gets an instanace of the Logger class
   *
   * @access public
   * @param mixed $class class name or class object
   * @static
   */  
  public static function getLogger($class) {
    Logger::$configuration = Configuration::getConfiguration();
    if (is_object($class)) { $class = get_class($class); }
    return new Logger($class);
  }
  
  /**
   * Constructor
   *
   * Logger class constructor
   *
   * @access private
   * @param mixed $class class name or class object
   */  
  private function __construct($className) {
    Logger::$configuration = Configuration::getConfiguration();

    if (Logger::$logFileHandle == null) {
      Logger::openLog();
    }
    
    $this->className = $className;
  }
  
  /**
   * Log
   *
   * Sends a message to the log.
   *
   * @access private
   * @param int $level
   * @param string $msg
   */
  private function log($level, $msg) {
    if (Logger::$configuration->environment['LOGGING']['file']['level'] <= $level) {

      Logger::openLog();
    
      fprintf(Logger::$logFileHandle, "[%s - %6s %s_%s - %29.29s %3s] %s\n",
	      date('h:i a'),
	      getmypid(),
	      $level,
	      Logger::$levels[$level],
	      $this->className,
	      str_repeat("+", $level),
	      $msg);		
      
      Logger::closeLog();
    }
    
  }

  /**
   * Open Log
   *
   * Opens the log file handle.
   *
   * @access private
   * @static
   */
  private static function openLog() {
    $path = Logger::$configuration->environment['LOGGING']['file']['path'] .
            Logger::$configuration->environment['LOGGING']['file']['name'];
    
    try {
      Logger::$logFileHandle = fopen($path, 'a');
    } catch (Exception $e) {
      die("Exception opening log file: " . $e->getMessage() . "\n");
    }
  }
  
  /**
   * Close Log
   *
   * Closes the log file handle.
   *
   * @access private
   * @static
   */
  private static function closeLog() {
    fclose(Logger::$logFileHandle);
  }

  /**
   * PHP Error
   *
   * Logging PHP errors.
   *
   * @access public
   * @param int $code
   * @param string $string
   * @param string $file
   * @param int $line
   * @param string $context
   */
  public function phpError($code, $string, $file, $line, $context) {
    if($code != 8) {
      
      if (array_key_exists($code, Logger::$phpErrorType)) {
        $err_name = Logger::$phpErrorType[$code];
      } else {
        $err_name = 'CAUGHT EXCEPTION';
      }

      $format = "PHPERROR! Details Follow....
 ================================================================================
    ERROR TYPE: [%s]
        TYPE #: [%s]
       MESSAGE: [%s] 
          FROM: [%s] 
          LINE: [%s] 
  CONTEXT DUMP:
  %s
 ===============================================================================
";

      $context_display = $context;

      if($context_display['this']) {
	$context_display['this'] = '-- suppressed for brevity --';
      }

      if($context_display['controller']) {
	$context_display['controller'] = '-- suppressed for brevity --';
      }

      if($context_display['page']) {
	$context_display['page'] = '-- suppressed for brevity --';
      }

      $this->log(Logger::$ERROR, 
		 sprintf($format, $err_name, $code, $string, $file, $line, print_r($context_display,1)));
    }
    return true;
  }

  /**
   * Trace
   *
   * Sends a trace message to the log.
   *
   * @access public
   * @param string $msg
   */
  public function trace($msg) {
    $this->log(C_LOGGER_TRACE, $msg);
  }

  /**
   * Debug
   *
   * Sends a debug message to the log.
   *
   * @access public
   * @param string $msg
   */
  public function debug($msg) {
    $this->log(C_LOGGER_DEBUG, $msg);
  }

  /**
   * Info
   *
   * Sends an info message to the log.
   *
   * @access public
   * @param string $msg
   */
  public function info($msg) {
    $this->log(C_LOGGER_INFO, $msg);
  }

  /**
   * Error
   *
   * Sends an error message to the log.
   *
   * @access public
   * @param string $msg
   */
  public function error($msg) {
    $this->log(C_LOGGER_ERROR, $msg);
  }

  /**
   * Fatal
   *
   * Sends a fatal message to the log.
   *
   * @access public
   * @param string $msg
   */
  public function fatal($msg) {
    $this->log(C_LOGGER_FATAL, $msg);
    die;
  }
}

