<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Database.php
 * 
 * Contains the abstract {@link Database} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract Database Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class Database extends Data {

  /**
   * Type
   *
   * Holds the Database class type
   *
   * @var string $type
   * @access public
   */  
  public $type;

  /**
   * Database Configuration
   *
   * Holds the Database Configuration hash
   *
   * Example:
   * <pre>
   * $dbConfig['class'];     // "MySQL"
   * $dbConfig['username'];  // xxx
   * $dbConfig['password'];  // xxx
   * $dbConfig['database'];  // test_schema
   * $dbConfig['host'];      // db.example.com
   * </pre>
   *
   * @var Array $config
   * @access public
   */  
  public $dbConfig;

  /**
   * Connect
   *
   * Create and return a database connection.
   *
   * @access protected
   * @returns resource $connection
   */
  abstract protected function connect();

  /**
   * Disconnect
   *
   * Close an existing database connection.
   *
   * @param resource $connection
   * @access protected
   */
  abstract protected function disconnect($connection);

  /**
   * Now
   *
   * Return date-time in format required by DB.  
   * This will default to the default format used by the database, but may be overridden in the config.
   *
   * @access public
   * @returns string The current data and time.
   */
  abstract public function now();

  /**
   * Execute
   *
   * Execute a non query operation on the database (typically: insert, update, delete)
   *
   * @param string $statement SQL statement
   * @access public
   * @returns int $result number of database rows affected
   */
  abstract public function execute($statement);

  /**
   * Query
   *
   * Execute a query operation on the database (typically: select)
   *
   * @param string $statement
   * @access public
   * @returns array $result_set 
   */
  abstract public function query($statement);

  /**
   * Smart Insert
   *
   * Execute an insert operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $valuesHash
   * @access public
   * @returns array $result
   */
  abstract public function smartInsert($model, $valuesHash);

  /**
   * Smart Update
   *
   * Execute an update operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $setHash
   * @param array $whereHash
   * @access public
   * @returns array $result
   */
  abstract public function smartUpdate($model, $setHash, $whereHash);

  /**
   * Smart Delete
   *
   * Execute a delete operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $whereHash
   * @access public
   * @returns array $result
   */
  abstract public function smartDelete($model, $whereHash);

  /**
   * Smart Select
   *
   * Execute a select operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $whatHash
   * @param array $whereHash
   * @param string $addendum
   * @access public
   * @returns array $result
   */
  abstract public function smartSelect($model, $whatHash, $whereHash, $addendum);

  /**
   * Smart Select Paginated
   *
   * Execute a paginated select operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $whereHash
   * @param array $orderHash
   * @param int $page
   * @param int $perPage
   * @access public
   * @returns array $result
   */
  abstract public function smartSelectPaginated($model, $page, $perPage, $whereHash=null, $orderHash=null);

  /**
   * Smart Select Conditions
   *
   *
   * @param Model $model
   * @param array $whereHash
   * @param array $orderHash
   * @access public
   * @returns array $result
   */
  abstract public function smartSelectConditions($model, $whereHash=null, $orderHash=null);

}
