<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Service.php
 * 
 * Contains the abstract {@link Service} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract Service Class
 *
 * The Service class encapsulates the functions required to access an external REST 
 * service in support of a RestModel.  It is analogous to a DataBase class'
 * support of a DatabaseModel.
 *
 * See the {@link local:docs/r.php?t=&s=services Services} section of the framework manual. 
 *
 * @package SP5
 * @subpackage core
 */
abstract class Service extends App {

  /**
   * Type
   *
   * Holds the service class type
   *
   * @var string $type
   * @access public
   */
  public $type;

  /**
   * Service Configuration
   *
   * Holds the Service Configuration hash
   *
   * Example:
   * <pre>
   * $dbConfig['class'];     // "Action"
   * $dbConfig['host'];      // www.example.com
   * </pre>
   *
   * @var Array $config
   * @access public
   */
  public $serviceConfig;

  /**
   * Constructor
   *
   * @ignore
   * @access public
   */  
  public function __construct() {
    parent::__construct();
    $this->logger->trace("Instantiated " . get_class($this) . "...");
  }

  /**
   * Prepare String
   *
   * Prepare a string for insertion.
   *
   * @param string $columnValue
   * @param bool $addSlashes
   * @access public
   * @returns string $columnValue
   */
  public function prepareArgument($columnValue) {
    return urlencode($columnValue);
  }


}
