<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * MySQLModelBuilderPlugin.php 
 * 
 * Contains the {@link BuildModelsPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * Requires DatabaseModelBuilderPlugin.php
 *
 */
require_once(C_PATH_CORE_PLUGINS . 'DatabaseModelBuilderPlugin.php');

/**
 * The MySQLModelBuilderPlugin Class
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class MySQLModelBuilderPlugin extends DatabaseModelBuilderPlugin {

  /**
   * Get Tables
   *
   * return normalized table list (Mysql by default - override for other databases)
   *
   * @access protected
   * @returns array $tables
   */
  protected function getTables() {
    $rawTables = $this->db->query("SHOW TABLES FROM " . $this->dbInfo['database'] . ';');

    $tables = array();
    
    foreach ($rawTables as $t) {
      $tables[] = $t['Tables_in_' . $this->dbInfo['database']];
    }

    return $tables;
  }

  /**
   * Get Views
   *
   * return any views (supported in mysql > v5)
   *
   * @access protected
   * @returns array $tables
   */
  protected function getViews() {
    $rawTables = $this->db->query("SELECT table_name FROM information_schema.tables WHERE table_schema = '" . $this->dbInfo['database'] . "' AND table_type = 'VIEW';");

    $tables = array();
    
    foreach ($rawTables as $t) {
      $tables[] = $t['table_name'];
    }

    return $tables;
  }

  /**
   * Get Procedures
   *
   * @access protected
   * @returns array of procedures
   */
  protected function getProcedures() {
    $rawProcedures = $this->db->query('SHOW PROCEDURE STATUS WHERE `Db` = "' . $this->dbInfo['database'] . '";');
    $procedures    = array();
    
    foreach ($rawProcedures as $p) {
      $procedures[$p['Name']] = array('meta' => $p, 'args' => $this->parseProcedure($p['Name']));
    }

    return $procedures;
  }

  /**
   * Get Functions
   *
   * @access protected
   * @returns array of functions
   */
  protected function getFunctions() {
    $rawFunctions = $this->db->query('SHOW FUNCTION STATUS WHERE `Db` = "' . $this->dbInfo['database'] . '";');
    $functions    = array();
    
    foreach ($rawFunctions as $p) {
      $functions[$p['Name']] = array('meta' => $p, 'args' => $this->parseFunction($p['Name']));
    }

    return $functions;
  }

  /**
   * Parse Procedure
   *
   * @access protected
   * @param string $procName
   * @returns array procedure
   */
  protected function parseProcedure($procName) {
    return $argHash = $this->parseProcFunc($procName,'PROCEDURE');
  }

  /**
   * Parse Function
   *
   * @access protected
   * @param string $funcName
   * @returns array function
   */
  protected function parseFunction($procName) {
    return $argHash = $this->parseProcFunc($procName,'FUNCTION');
  }

  /**
   * Parse Procedure or Function
   *
   * @access protected
   * @param string $name
   * @param string $type [PROCEDURE]
   * @returns array procedure or function
   */
  protected function parseProcFunc($name, $type='PROCEDURE') {
    $rawProcedure = $this->db->query('SHOW CREATE ' . $type . ' `' . $name . '`;');
    $matches = array();

    // parenthesis recursion regex
    //
    preg_match('/\((([^()]+|(?R))*)\)/', 
	       $rawProcedure[0]['Create ' . ucfirst(strtolower($type))], 
	       $matches);

    $rawArgs = array();

    if(count($matches) > 2) {
      $rawArgs = explode(',',$matches[1]);
    }

    $argArray = array();
    $argHash  = array();


    foreach($rawArgs as $rawArg) {
      $argArray = preg_split("/\s+/", trim($rawArg));


      if(count($argArray) == 3) {
	if(strtolower($argArray[0]) == 'in' || strtolower($argArray[0]) == 'inout') {

	  $argHash[$argArray[1]] = array('arg' => $argArray[1],
					 'prep' => $this->prepareMethod($this->getColumnType(array('Type' => $argArray[2]))),
					 'dbType' => $argArray[2], 
					 'phpType' => $this->getPHPTypeByColumnType($this->getColumnType(array('Type' => $argArray[2]))),
					 'type' => $this->getColumnType(array('Type' => $argArray[2])));
	}
      }
      
      if(count($argArray) == 2) {
	$argHash[$argArray[0]] = array('arg' => $argArray[0], 
				       'prep' => $this->prepareMethod($this->getColumnType(array('Type' => $argArray[1]))),
				       'dbType' =>  $argArray[1], 
				       'phpType' => $this->getPHPTypeByColumnType($this->getColumnType(array('Type' => $argArray[1]))), 
				       'type' => $this->getColumnType(array('Type' => $argArray[1])));
      }
    }
    
    return $argHash;
  }

  /**
   * Prepare Method
   * 
   * @param string $type
   * @param mixed  $value
   * @access private
   * @returns string method used to prepare
   */
  private function prepareMethod($type) {

    if ($type == 's'  || $type == 'd'  ||
	$type == 'dt' || $type == 'tm' ||
	$type == 'e'  || $type == 't') {
      return "prepareString";
    } else if ($type == 'g' ) {
      return "prepareGeometric";
    } else {
      return "prepareNumber";
    }
  }

  /**
   * Get Columns
   *
   * return normalized column list (Mysql by default - override for other databases)
   *
   * @param string $table
   * @access protected
   * @returns array $columns
   */
  protected function getColumns($table) {
    $rawColumns = $this->db->query("SHOW COLUMNS FROM `" . $table . '`');

    $columns = array();

    foreach ($rawColumns as $c) {
      $col = array();
      $col['name'] = $this->getColumnName($c);
      $col['type'] = $this->getColumnType($c);
      $col['default'] = $this->getColumnDefault($c);
      $col['primary'] = $this->isColumnPrimaryKey($c);
      $col['notnull'] = $this->isColumnNotNull($c);
      $col['enums'] = $this->getEnumValues($c);
      
      if($c['Extra'] == 'auto_increment') {
	$col['auto_increment'] = $this->getColumnName($c);
      }

      $columns[] = $col;
    }

    return $columns;
  }

  /**
   * Get Column Name
   *
   * return normalized column name (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns string $name
   */
   protected function getColumnName($col) {
     return $col['Field'];
   }

   /**
    * Get Column Type
    *
    * return normalized column type (Mysql by default - override for other databases)
    *
    * TODO: This needs a lot more love
    *
    * @param array $col
    * @access protected
    * @returns string $type
    */
   protected function getColumnType($col) {
     if (strstr(strtolower($col['Type']), 'tinyint(1)')) {
       $type = "b";
     } else if (preg_match('/^int|^bigint|^tinyint/', strtolower($col['Type']))) {
       $type = "i";
     } else if (preg_match('/^char|^varchar/', strtolower($col['Type']))) {
       $type = "s";
     } else if (preg_match('/^text/', strtolower($col['Type']))) {
       $type = "t";
     } else if (preg_match('/^enum/', strtolower($col['Type']))) {
       $type = "e";
     } else if (preg_match('/^float/', strtolower($col['Type']))) {
       $type = "f";
     } else if (preg_match('/^date(?!time)/', strtolower($col['Type']))) {
       $type = "d";
     } else if (preg_match('/^time/', strtolower($col['Type']))) {
       $type = "tm";
     } else if (preg_match('/^datetime/', strtolower($col['Type']))) {
       $type = "dt";
     } else {
       $type = "s";
     }

     return $type;
   }

  /**
   * Get Column Default
   *
   * return normalized column default value (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns string $default
   */
   protected function getColumnDefault($col) {
     $def = $col['Default'];

     $type = $this->getColumnType($col);

     $def = (!$def && $type == "dt") ? '0000-00-00 00:00:00' : $def;
     $def = (!$def && $type == "tm") ? '00:00:00' : $def;
     $def = (!$def && $type == "d") ? '0000-00-00' : $def;

     return $def;
   }

  /**
   * Is Primary Key
   *
   * return normalized determination as to whether this is the primary key (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns boolean $isPrimary
   */
   protected function isColumnPrimaryKey($col) {
     return $col['Key'] == 'PRI' && strstr($col['Extra'], 'auto_increment') ? TRUE : FALSE;
   }

  /**
   * Is Column Not Null
   *
   * return normalized not null flag (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns boolean $isNotNull
   */
   protected function isColumnNotNull($col) {
     return $col['Null'] == 'NO' ? TRUE : FALSE;
   }

  /**
   * Get Enum Values
   *
   * return normalized enumeration values (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns array $enums
   */
   protected function getEnumValues($col) {
     if ($this->getColumnType($col) != "e") { return null; }

     preg_match("|(\'.*\')|", $col['Type'], $out);

     eval("\$tmp_enums = array(" . $out[1] . ");");

     return $tmp_enums;
   }

}
