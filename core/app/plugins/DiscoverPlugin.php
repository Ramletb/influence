<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * DiscoverPlugin.php 
 *
 * Contains the {@link DiscoverPlugin} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The DiscoverPlugin Class
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class DiscoverPlugin extends Plugin {

  /**
   * Parse Source
   *
   * Parse a Controllers source.
   *
   * @param array $filePaths Paths to classes
   * @param array $cleanActions an array of (prefix free) action names
   * @param string $prefix
   * @param bool $bundle are we parsing a bundle?
   $ @returns array An array of discovery data.
   * @access public
   */  
  private function parseSource($filePaths, $cleanActions, $prefix, $bundle=false) {

    $foundActions = array();
    $sourceArray  = array();

    // parse the source
    //
    foreach($filePaths as $filePath) {
      $sourceArray = array_merge($sourceArray, file($filePath));
    }

    $sourceArray = array_reverse($sourceArray);

    $actionInterface = array();
    $actionMethod    = array();
    $currentAction   = false;

    foreach($sourceArray as $sourceLine) {

      $methodPattern = "/function\s+\w+\(.*\{/";

      if(preg_match($methodPattern, $sourceLine)) {
	$currentAction = false;
      }


      foreach($cleanActions as $action => $val) {

	if($bundle) {
	  $actionPattern = "/function\s+" . $action . "\(/";
	} else {
	  $actionPattern = "/function\s+" . $prefix . '_' . $action . "\(/";
	}

	if(preg_match($actionPattern, $sourceLine, $m)) {
	  $foundActions[$action] = true;
	  $currentAction = $action;
	}

      }

      $paramData = $this->matchParameter($sourceLine);
      
      if(is_array($paramData) && $currentAction) {

	if(array_key_exists($currentAction, $actionInterface) && !is_array($actionInterface[$currentAction])) {
	  $actionInterface[$currentAction] = array();
	}
	
	if(preg_match('/(\w+)Bundle/',$paramData['pType'],$m)) {
	  $bundleData = $this->parseSource(array($this->findBundle($m[0])), array('setBundle' => 1), '', true);

	  foreach($bundleData['actionMethod']['setBundle'] as $am => $amv) {
	    foreach($amv as $p => $v) {
	      $actionMethod[$currentAction][$am][$p] = $v;
	    }
	  }

	  foreach($bundleData['actionInterface']['setBundle'] as $ai => $v) {
	    $actionInterface[$currentAction][$ai] = $v;
	  }

	} else {

	  $actionMethod[$currentAction][$paramData['proto']][$paramData['name']]['type']  = $paramData['type'];
	  $actionMethod[$currentAction][$paramData['proto']][$paramData['name']]['desc']  = $paramData['desc'];
	  
	  $actionInterface[$currentAction][$paramData['name']]['proto'] = $paramData['proto'];
	  $actionInterface[$currentAction][$paramData['name']]['type']  = $paramData['type'];
	  $actionInterface[$currentAction][$paramData['name']]['desc']  = $paramData['desc'];
	}

      }
    }

    $data['actionMethod']    = $actionMethod;
    $data['actionInterface'] = $actionInterface;

    return $data;
  }

  /**
   * Match Parameter
   *
   * @param string $sourceLine
   $ @returns array|bool
   * @access public
   */  
  private function matchParameter($sourceLine) {
    $pattern = '/\*\s+\@param\s+(\w+)\s+\$(\w+)\s+(\w+)\s+(\w+)\s+(.*)/';

    if(preg_match($pattern, $sourceLine, $m)) {

      return array('pType' => $m[1],
		   'name'  => $m[2],
		   'proto' => $m[3],
		   'type'  => $m[4],
		   'desc'  => $m[5]);
      } else {
	return false;
      }
  }

  /**
   * Find Bundle
   *
   * @param string $bundleName the name of the bundle
   $ @returns string path to bundle
   * @access public
   */  
  private function findBundle($bundleName) {
    $path = C_PATH_BUNDLES . $bundleName . '.php';

    if(!is_file($path)) {
      $path = C_PATH_CORE_BUNDLES . $bundleName . '.php';
      if(!is_file($path)) {
	$path = false;
      }
    }

    return $path;
  }

  /**
   * Discover Actions
   *
   * Discover actions within a controller.
   *
   * @param Controller $controllerObject
   * @param string $prefix
   $ @returns array An array of discovery data.
   * @access public
   */  
  public function discoverActions($controllerObject, $prefix) {

    // get all the methods for controller
    // some may be inherited
    //
    $methods = get_class_methods($controllerObject);

    // get the names of all the ancestors
    //
    $ancestors = $this->getAncestors($controllerObject);
    array_shift($ancestors);
    $ancestorPaths = array();
    array_push($ancestorPaths, $controllerObject->getFilePath());

    foreach($ancestors as $ancestor) {
      $ancestorFile = $ancestor . '.php';
      //
      // check the app dir
      $ancestorPath = C_PATH_CONTROLLERS . $ancestorFile;
      if(is_file($ancestorPath)) {
	array_push($ancestorPaths, $ancestorPath);
	continue;
      }
      //
      // check the core app dir
      $ancestorPath = C_PATH_CORE_CONTROLLERS . $ancestorFile;
      if(is_file($ancestorPath)) {
	array_push($ancestorPaths, $ancestorPath);
	continue;
      }

      //
      // check the core dir
      $ancestorPath = '../core/' . $ancestorFile;
      if(is_file($ancestorPath)) {
	array_push($ancestorPaths, $ancestorPath);
	continue;
      }      

    }

    $actions = array();
    $prefixLen = strlen($prefix);

    foreach($methods as $method) {
      if(substr($method, 0, $prefixLen) == $prefix) {
	array_push($actions, $method);
      }
    }

    $cleanActions = array();

    foreach($actions as $action) {
      $a = substr($action, ($prefixLen + 1));
      $cleanActions[$a] = true;
    }


    $data = array();

    $data = $this->parseSource($ancestorPaths, $cleanActions, $prefix);

    $data['action'] = $cleanActions;

    return $data;
  }

  /**
   * Get Ancestors
   *
   * Get the ancestors of a given class.
   *
   * @param string $class
   $ @returns array An array of ancestors.
   * @access private
   */  
  private function getAncestors($class) {
    
    for ($classes[] = $class; $class = get_parent_class($class); $classes[] = $class);
    return $classes;
    
  }

}

