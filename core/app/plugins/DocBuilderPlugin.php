<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * DocBuilderPlugin.php 
 * 
 * Contains the {@link DocBuilderPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The DocBuilderPlugin Class
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class DocBuilderPlugin extends Plugin {

  /**
   * @access private
   * @var object $outputCallbackObj;
   */
  private $outputCallbackObj;

  /**
   * @access private
   * @var object $outputCallbackMethod;
   */
  private $outputCallbackMethod;

  /**
   * Set Output Callback
   *
   * @access public
   * @param object $obj
   * @param string $method
   */
  public function setOutputCallback($obj,$method) {
    $this->outputCallbackObj = $obj;
    $this->outputCallbackMethod = $method;
  }

  /**
   * Output a message
   *
   * @access private
   * @param string $msg
   */
  private function output($msg) {
    $obj = $this->outputCallbackObj;
    $method = $this->outputCallbackMethod;

    $obj->$method($msg);
  }

  /**
   * Generate Documentation
   *
   * @access public
   * @param string $package
   */
  public function generate($package) {

    $path = $_ENV['PWD'] . '/../tutorials/';

    if(!file_exists($path)) {
      mkdir($path, 0777, true);
    }

    $tutorialsDir = $path;
    $docsDir = $_ENV['PWD'] . '/../www/docs/';

    system('rm -rf ' . $tutorialsDir . '*');

    if(!file_exists($tutorialsDir . $package)) { 
      mkdir($tutorialsDir . $package, 0777, true);
    }

    $docbookRenderer = $this->rendererFactory->load('DocBook');
    $docbookRenderer->addMetaData('webserver', $this->configuration->environment['FRAMEWORK']['web']['server']);
    $docbookRenderer->addMetaData('webPath', $this->configuration->environment['FRAMEWORK']['web']['path']);
    $docbookRenderer->addMetaData('package', $package);

    $fileArray = scandir($_ENV['PWD'] . '/../core/app/views/build_docs/docbook/');

    foreach($fileArray as $file) {
      list($view, $ext) = explode('.', $file, 2);
      list($prefix, $suffix) = explode('_', $view, 2);
      
      $contents = '';

      if($prefix == 'pkg') {
	$contents = $docbookRenderer->renderReturn('core/build_docs/'.$view);
	if($suffix == 'index') {
	  file_put_contents($tutorialsDir . $package . '/' . $package . '.pkg', $contents);
	} else {
	  file_put_contents($tutorialsDir . $package . '/' . $package . '.' . $suffix . '.pkg', $contents);	  
	}
      }

      if($prefix == 'pkgini') {
	$contents = $docbookRenderer->renderReturn('core/build_docs/'.$view);
	if($suffix == 'index') {
	  file_put_contents($tutorialsDir . $package . '/' . $package . '.pkg.ini', $contents);
	} else {
	  file_put_contents($tutorialsDir . $package . '/' . $package . '.' . $suffix . '.pkg.ini', $contents);	  
	}
      }
    }

    $cmd  = '';
    $cmd  = './phpdoc/phpdoc';
    $cmd .= ' -t ' . $_ENV['PWD'] . '/../www/docs ';
    $cmd .= ' -d ' . $_ENV['PWD'] . '/../';
    $cmd .= ' -i docs/,logs/,pear/,utils/doc/,utils/phpdoc/,lib/';
    $cmd .= ' -dn ' . $package;
    $cmd .= ' -ue on';
    $cmd .= ' -ct payload';
    $cmd .= ' -ct clientdata';
    $cmd .= ' -ti Documentation';
    $cmd .= ' -o HTML:frames:phphtmllib';
    //$cmd .= ' -o PDF:default:default';

    //echo $cmd . "\n";

    $handle = popen($cmd . ' 2>&1', 'r');

    while (!feof($handle)) {
      $this->output(rtrim(fread($handle, 8192)));
    }

    pclose($handle);
    
    $rfile = '';

    $rfile .= '<?php'. "\n";
    $rfile .= '$tutorial = $_GET[\'t\'];'. "\n";
    $rfile .= '$section  = $_GET[\'s\'];'. "\n";
    $rfile .= 'if(!$tutorial) {'. "\n";
    $rfile .= '  $url = \'/docs/' . $package . '/tutorial_' . $package . '.pkg.html#\' . $section;'. "\n";
    $rfile .= ' } else {'. "\n";
    $rfile .= '  $url = \'/docs/' . $package . '/tutorial_' . $package . '.\' . $tutorial . \'.pkg.html#\'. $section;'. "\n";
    $rfile .= ' }'. "\n";
    $rfile .= '?>'. "\n";
    $rfile .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'. "\n";
    $rfile .= '<html>'. "\n";
    $rfile .= '<head>'. "\n";
    $rfile .= '<title>Framework Documentation</title>'. "\n";
    $rfile .= '<meta http-equiv="REFRESH" content="0;url=<?php echo $url; ?>">'. "\n";
    $rfile .= '<script type="text/javascript">'. "\n";
    $rfile .= '   window.location = "<?php echo $url; ?>";'. "\n";
    $rfile .= '</script>'. "\n";
    $rfile .= '</head>'. "\n";
    $rfile .= '<body>'. "\n";
    $rfile .= 'Redirecting...'. "\n";
    $rfile .= '</body>'. "\n";
    $rfile .= '</html>'. "\n";

    file_put_contents($docsDir . 'r.php', $rfile);

    $hta = '';
    $hta .= '# General Apache options' . "\n";
    $hta .= '#' . "\n";
    $hta .= 'RewriteEngine Off' . "\n";

    file_put_contents($docsDir . '.htaccess', $hta);

  }

}
