<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Configuration Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id configuration}">
  <refnamediv>
    <refname>Configuration</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}
  <refsect1 id="{@id configuration_overview}">
    <title>Overview</title>
<para>
Framework configuration is divided into three 
categories, <important>environment</important>, <important>application</important> and <important>framework</important>. 
Configuration files can be found in the <important>./config/</important> directory.
</para>
<para>
Each configuration file is prefixed with either <important>default_</important>, 
<important>custom_</important> or <important>core_</important>. Configuration 
files prefixed with <important>core_</important> or <important>default_</important> 
should not be modified and may be overwritten with framework updates.
</para>
<para>
Any configuration prefixed with custom_ will override values found in default_ 
prefixed files. The framework will check custom_ files first, any entry that is 
not found will be read from a default_ configuration file.
</para>
    <para>
    <graphic src="../../images/core/config.png"></graphic>
    </para>

  <refsect2 id="{@id configuration.environment}">
    <title>Environment</title>

<para>
Environment configuration files (*_environment.ini) are intended to hold information 
specific to the environment as opposed to the application. 
For example, a framework running in a development 
environment would contain an entry <important>"development.mode = true"</important>
whereas it's live counterpart's entry would state 
<important>"development.mode = false"</important>. Database 
connection information, logging levels, and environment specific 
paths belong in the environment configuration files. Custom 
environment configuration files are not intended to be pushed 
with application releases. 
</para>
   </refsect2>

  <refsect2 id="{@id configuration.application}">
    <title>Application</title>
<para>
Application configuration files (*_application.ini) are intended 
to hold information used for the application regardless of it's 
environment. For instance the title of an application is the same 
wether on a live or development server.
</para>
   </refsect2>

  <refsect2 id="{@id configuration.implementation}">
    <title>Implementation</title>
    <para>
The {@link Configuration} class is instantiated in deep in 
framework's {@link Core} class providing access to a 
$configuration object from any framework class. Access to the 
configuration object available to Controllers, Plugins, 
Parsers, Renderers and Models through the $this->configuration 
instance variable.
    </para>
    <para>
See the diagram below for an example on how to access the current development mode through the configuration object:
    </para>
    <para>
    <graphic src="../../images/core/configMap.png"></graphic>
    </para>

<para>
The $this->configuration object provides access to two arrays, 
$this->configuration-application and $this->configuration->environment 
representing *_application.ini and *_environment.ini respectively. 
</para>

   </refsect2>

  </refsect1>

</refentry>