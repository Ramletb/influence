<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Transformation Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_transformation}">
  <refnamediv>
    <refname>RSS and Transformation</refname>
    <refpurpose></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_transformation_key_concepts}">
      <title>Key Concepts</title>
      <para>
Whether you have noticed or not, until now we have only 
been utilizing Presentation and Serialization Renderers 
in our Controller output. Dump, XHTML and Text are Presentation 
Renderers, while JSON, SPHP and XML are Serialization Renderers. 
In this tutorial we will look at the best example of a 
Transformation Renderer, the RSSRenderer.
      </para>
      <para>
RSS is not a markup language or a serialization syntax. 
RSS is an XML specification for describing data. The RSSRenderer 
is able to generate well-formed valid RSS without forcing the 
developer to hand code and validate chunks of XML.
      </para>
      <para>
Transformation renderers use views; however, unlike their counterparts 
Presentation Renderers, a Transformation Renderer's view is 
a class and not a flat php script. Transformation renderers consume 
these specialized classes intended to help them "transform" the 
incoming data.
      </para>
    </refsect1>    
    <refsect1 id="{@id tutorial_transformation_providing_data}">
      <title>Providing Data</title>
      <para>
Before we can generate RSS we need data. Add the following action 
to your HelloController.
      </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
/**
 * Web feed
 *
 * @access public
 */
public function web_feed() {

  $this->addMetaData("title","A Bunch of Nonsense");

  $data = new stdClass();
  
  $data->nonsense = array();
  $data->nonsense[0]['user']   = "Adam";
  $data->nonsense[0]['advise'] = "Proin a tortor quis ligula egestas convallis.";
  $data->nonsense[1]['user']   = "Brian";
  $data->nonsense[1]['advise'] = "Sed vitae nulla a nisl vestibulum aliquet.";
  $data->nonsense[2]['user']   = "Craig";
  $data->nonsense[2]['advise'] = "Lorem ipsum dolor sit amet, adipiscing elit.";
  
  $this->setData($data);
  
  $this->render('hello/feed');
}
  ]]>
    </programlisting>
    </para>
    <para>
We may want to provide the same data to Flash, Javascript or another 
PHP based system using the same action. Lets take a look at the output 
of this data in other formats before we write our RSS Transformation class.
    </para>
    <para>
Rendering JSON:
    </para>
    <para>
    <browserurl>
    http://<?php echo $webserver; ?>/hello/feed.json
    </browserurl>
    </para>
    <para>
    <browserscreen>
//{"nonsense":[{"user":"Adam","advise":"Proin a tortor quis ligula egestas convallis."},{"user":"Brian","advise":"Sed vitae nulla a nisl vestibulum aliquet."},{"user":"Craig","advise":"Lorem ipsum dolor sit amet, adipiscing elit."}]}
    </browserscreen>
    </para>
    <para>
Rendering SPHP:
    </para>
    <para>
    <browserurl>
    http://<?php echo $webserver; ?>/hello/feed.sphp
    </browserurl>
    </para>
    <para>
    <browserscreen>
    O:8:"stdClass":1:{s:8:"nonsense";a:3:{i:0;a:2:{s:4:"user";s:4:"Adam";s:6:"advise";s:45:"Proin a tortor quis ligula egestas convallis.";}i:1;a:2:{s:4:"user";s:5:"Brian";s:6:"advise";s:42:"Sed vitae nulla a nisl vestibulum aliquet.";}i:2;a:2:{s:4:"user";s:5:"Craig";s:6:"advise";s:44:"Lorem ipsum dolor sit amet, adipiscing elit.";}}}
    </browserscreen>
    </para>
    <para>
Rendering Dump is a nice way to verify both our data and meta data:
    </para>
    <para>
    <browserurl>
    http://<?php echo $webserver; ?>/hello/feed.dump
    </browserurl>
    </para>
    <para>
    <browserscreen>
    <graphic src="../../images/core/feed_dump.png"></graphic>
    </browserscreen>
    </para>
    </refsect1>    
    <refsect1 id="{@id tutorial_transformation_rsstransformer}">
    <title>RSSTransformer Class</title>
    <para>
Now that we have some data let's write an RSSTransformer class. 
We can start by copying the ExampleRssTransformer into the appropriate 
View directory.
    </para>
    <para>
Make an "rss" View directory under "hello" View directory.
    </para>
    <para>
    <screen>
$ cd ./app/views/hello/
    </screen>
    </para>
    <para>
    <screen>
$ mkdir rss; cd rss
    </screen>
    </para>
    <para>
    <screen>
$ cp ../../../../core/app/views/default/rss/ExampleRssTransformer.php .
    </screen>
    </para>
    <para>
    <screen>
$ cp ExampleRssTransformer.php HelloFeedRssTransformer.php
    </screen>
    </para>
    <para>
The following diagram shows how the name is determined for 
transformation classes. The class and file must have the same 
name to be recognized by the Framework.
    </para>
    <para>
    <graphic src="../../images/core/hello_feed_diagram.png"></graphic>
    </para>
    <para>
Open your new HelloFeedRssTransformer in a text editor.
    </para>
    <para>
    <screen>
$ vi HelloFeedRssTransformer.php
    </screen>
    </para>
    <para>
Edit your HelloFeedRssTransformer.php so it resembles the following code:
    </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
/*
/ Framework Tutorial Project
*/

/**
 * HelloFeedRssTransformer.php
 *
 * Contains the {@}link HelloFeedTransformer} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */

/**
 * The HelloFeedRssTransformer Class
 *
 *
 * @package SP5
 * @subpackage transformer
 */
class HelloFeedRssTransformer extends RSSTransformer {

  /**
   * Get Channel Title (REQUIRED)
   *
   * Returns the RSS channel title.
   *
   * @access public 
   * @returns string RSS channel title
   */
  public function getTitle() {
    return $this->metaData['title'] . ' Feed';
  }

  /**
   * Get Channel Link (REQUIRED)
   *
   * Returns the RSS channel link.
   *
   * @access public 
   * @returns string RSS channel link (URL.)
   */
  public function getLink() {
    $link  = 'http://';
    $link .= $this->configuration->environment['FRAMEWORK']['web']['server'];
    $link .= '/hello/feed';
    return $link;
  }

  /**
   * Get Channel Items
   *
   * Returns an array of {@}link RSSTransformerItem}s.
   *
   * @uses RSSTransformerItem
   * @access public 
   * @returns array an array of {@}link RSSTransformerItem}s.
   */
   public function getItems() {
     $items = array();

     foreach($this->data->nonsense as $entry) {
       $item = new RSSTransformerItem;
       $item->title = $entry['user'] . "'s Advice";
       $item->description = $entry['advise'];

       array_push($items, $item);
     }

     return $items;
   }
}
  ]]>
    </programlisting>
    </para>
    <para>
The three methods, getTitle, getLink, and getItems provide the 
RssTransformer the minimal information required to generate a 
simple RSS feed. 
    </para>
    <para>
Since most browsers attempt to open RSS in a feed reader we will 
add a CLI wrapper action to our HelloController in order to see 
the resulting RSS on the terminal. Add the following Action to
    your HelloController:
    </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
/**
 * Cli feed
 *
 * Wraps the web_feed action
 *
 * @access public
 */
public function cli_feed() {
  $this->web_feed();
}
  ]]>
    </programlisting>
    </para>
    <para>
    Execute the following command to see the generated RSS output:
    </para>
    <para>
    <screen>
$ cd ./utils/; ./cli.php --controller hello --action feed --format rss
    </screen>
    </para>
    <para>
The above command generated the following RSS output:
    </para>
    <para>
    <screen>
    <graphic src="../../images/core/rss_output.png"></graphic>
    </screen>
    </para>
    <para>
As you can see the output is fairly generic. Add the following 
method to your HelloFeedRssRenderer class:
    </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
/**
 * Get Namespace Hash
 *
 * Returns a hash of namespaces and urls
 * 'dc' => 'http://purl.org/dc/elements/1.1/'
 *
 * @see http://validator.w3.org/feed/docs/howto/declare_namespaces.html
 * @access public 
 * @returns array xml namespace array
 */
public function getNamespaces() {
  $ns = array();
  $ns['dc'] = 'http://purl.org/dc/elements/1.1/';

  return $ns;
}
]]>
    </programlisting>
    </para>
    <para>
The method above supplies the RSS Renderer with a hash of additional 
RSS Namespaces we plan to use. For an useful list of RSS Namespaces 
visit {@link http://validator.w3.org/feed/docs/howto/declare_namespaces.html}.
    </para>
    <para>
We will now need to supply the $item object with an attribute called customElement 
pointing to an array of custom elements to be added to our feed item. At 
this point we only have one custom element, the dc:creator.
    </para>
    <para>
       Adjust your getItems method to reflect the following (note lines 18 and 19):
    </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
/**
 * Get Channel Items
 *
 * Returns an array of {@}link RSSTransformerItem}s.
 *
 * @uses RSSTransformerItem
 * @access public 
 * @returns array an array of {@}link RSSTransformerItem}s.
 */
public function getItems() {
  $items = array();
  
  foreach($this->data->nonsense as $entry) {
    $item = new RSSTransformerItem;
    $item->title = $entry['user'] . "'s Advice";
    $item->description = $entry['advise'];
    
    $creator = '<dc:creator>'.$entry['user'].'</dc:creator>';
    $item->customElements = array($creator);
    
    array_push($items, $item);
  }
  
  return $items;
}
]]>
    </programlisting>
    </para>     
    <para>
       Execute the following command to see the updated RSS output with the additional dc:creator element:
    </para>
    <para>
    <screen>
$ cd ./utils/; ./cli.php --controller hello --action feed --format rss
    </screen>
    </para>
    <para>
    <screen>
    <graphic src="../../images/core/rss_dc_output.png"></graphic>
    </screen>
    </para>

    </refsect1>    


</refentry>

