<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Models Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_models}">
  <refnamediv>
    <refname>Databases and Models</refname>
    <refpurpose></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_models_key_concepts}">
      <title>Key Concepts</title>
      <para>
In the context of the Framework, Models represent the domain layer. 
Plugins share this distinction with Models and both should be considered 
in a Domain-driven design. Domain level objects such as Models and Plugins 
should form a common language for describing business logic.
      </para>
      <para>
Framework Models are capable of "wrapping" local and remote services, 
other frameworks and more commonly, databases. In this tutorial we will 
focus on building Models utilizing a database.
      </para>
    </refsect1>
    
    <refsect1 id="{@id tutorial_models_configure}">
      <title>Configure</title>
      <para>
We will begin by setting up a database, adding a table and configuring the Framework. 
 First set up a database locally or on a server with appropriate permissions. 
      </para>
    <para>
Create a database table with the following create statement. You
may need to adjust the syntax if you are not using MySql.
    </para>
    <para>
    <screen>
CREATE TABLE `hello_user` (
   `id`       int(11) NOT NULL auto_increment,
   `username` char(255) default NULL,
   `password` char(255) default NULL,
   `bio`      text,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
    </screen>
    </para>
    <para>
Next we will need to add our database connection parameters to 
the Framework configuration.
    </para>
    <para>
Open "./config/custom_environment.ini" in a text editor. Create the file if
it does not already exist.
    </para>
    <screen>
$ vi ./config/custom_environment.ini
    </screen>
    <para>
Add the appropriate connection properties to allow the Framework
access to the database containing your new table.
    </para>
    <para>
    <screen>
[TUTORIAL]

db.class          = "MySQL"
db.username       = "username"
db.password       = "password"
db.database       = "hello"
db.host           = "localhost"
db.prepend        = true
db.format.charset = "utf-8"
    </screen>
    </para>
    </refsect1>

    <refsect1 id="{@id tutorial_models_build}">
    <title>Build Models</title>
    <para>
Database Models consist of generated code describing the structure 
of the database. This generated code allows Models to access data 
binding and table information without the need to continuously query 
the database for schema descriptions. While this method saves numerous 
database connections requesting the same information, you will 
however need to re-build models whenever the structure or your database 
changes. Fortunately generating Models is as easy as running the 
following command:
    </para>
    <para>
    <screen>
    $ cd ./utils/; ./cli.php --action build_models --package TUTORIAL --header ./doc/SAMPLE.header
    </screen>
    </para>
    <para>
Observe the following output:
    </para>
    <para>
    <screen>
Creating ../app/models/TUTORIAL/generated/TUTORIAL_HelloUserStructure.php...
Creating ../app/models/TUTORIAL/TUTORIAL_HelloUserModel.php...
    </screen>
    </para>
    <para>
Because our database only contains one table, only one Model and 
one Structure are generated. The "TUTORIAL_HelloUserModel.php" is 
where we will be developing business logic related to the "hello_user" 
database table.
    </para>
    <para>
Structures (and any other files) in the "/generated/" directory should 
not be edited and may be overwritten at any time. For instance if the 
structure of the "hello_user" table changes and models are rebuilt, these 
file will be overwritten. We are free to re-build models at anytime, running 
the "build_models" action again will produce the following result:    
    </para>
    <para>
    <screen>
Creating ../app/models/TUTORIAL/generated/TUTORIAL_HelloUserStructure.php...
Skipping ../app/models/TUTORIAL/TUTORIAL_HelloUserModel.php (It exists)...
    </screen>
    </para>
    <para>
Notice that the Model builder skipped generating the file 
"TUTORIAL_HelloUserModel.php"; since the file exists it may already 
contain implementation written by the developer. In other words you may 
safely modify your database and re-run "build_models" as often as necessary. 
Of course you may need to modify your implementation code to reflect major 
database changes.
    </para>
    </refsect1>
    <refsect1 id="{@id tutorial_models_developing}">
    <title>Developing a Model</title>
    <para>
    Start by open the new "TUTORIAL_HelloUserModel.php" file in a text editor.
    </para>
    <para>
    <screen>
$ vi ./app/models/TUTORIAL/TUTORIAL_HelloUserModel.php
    </screen>
    </para>
    <para>
The file should appear as follows:
    </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
<?php echo "<?php\n"; ?>
 /*
 /
 / $LastChangedBy: CraigJohnston $
 / $LastChangedDate: 2010-07-14 12:20:21 -0700 (Wed, 14 Jul 2010) $
 / $Rev: 2 $
 /
 / 001
*/
 
/** 
 * TUTORIAL HelloUser Model
 * 
 * @version GENERATED by ModelBuilderPlugin.php 
 * @package SP5
 * @subpackage models 
 */ 

 /**
 * Requires structure
 */
require_once('../app/models/TUTORIAL/generated/TUTORIAL_HelloUserStructure.php');

/**
* TUTORIAL_HelloUserModel class
*
* @package TUTORIAL
* @subpackage models
*/
class TUTORIAL_HelloUserModel extends TUTORIAL_HelloUserStructure {
}

<?php echo "?>\n"; ?>
]]>
    </programlisting>
    </para>
    <para>
Writing our first bit of implementation we will add a setter method 
used for adding a new user to our "hello_user" table. Add the following 
method to the TUTORIAL_HelloUserModel class.
    </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
/**
 * Add User
 *
 * Add a user record.
 *
 * @access public
 * @param string $username
 * @param string $password (optional)
 * @param string $bio      (optional)
 * @returns int the new record id.
 */
public function addUser($username, $password="",$bio="") {

  $recordId = $this->smartInsert(
				 array('username' => $username,
				       'password' => $password,
				       'bio'      => $bio)
				 );
  
  return $recordId;
}
  ]]>
    </programlisting>
    </para>
    </refsect1>
    <refsect1 id="{@id tutorial_models_using}">
    <title>Using Models</title>

    <para>
We can now use our new Model to add a user to the database. 
But first we need to add an Action to our controller. Add the 
following "web_add_user" Action to your HelloController:
    </para>
    <para>
    <programlisting role="php">
	  <![CDATA[
/**
 * Web add user
 *
 * @access public
 * @param PayloadPkg $username GET string username
 * @param PayloadPkg $password GET string password
 * @param PayloadPkg $bio      GET string bio
 */
public function web_add_user(PayloadPkg $username, 
			     PayloadPkg $password, 
			     PayloadPkg $bio) {

  $userModel = $this->modelFactory->load("HelloUser", "TUTORIAL");
  
  $recordId = $userModel->addUser(
				  (string) $username, 
				  (string) $password, 
				  (string) $bio
				  );

  $data = new stdClass();
  $data->newUserRecord = $recordId;
  
  $this->setData($data);
  
  $this->render();
}

  ]]>
    </programlisting>
    </para>

    <para>
To see the results of our new Action we can now go to the following url:
    </para>

    <para>
    <browserurl>
    http://<?php echo $webserver; ?>/hello/add_user.dump?username=craig
    </browserurl>
    </para>

    <para>
If the new user was successfully added to the database we 
will end up with the following results:
    </para>

    <para>
    <browserscreen>
    <graphic src="../../images/core/add_user_dump.png"></graphic>
    </browserscreen>
    </para>


    </refsect1>
    
</refentry>

