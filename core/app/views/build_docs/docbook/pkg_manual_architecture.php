<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Framework Architecture Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id architecture}">
  <refnamediv>
    <refname>Architecture</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}

  <refsect1 id="{@id architecture_overview}">
    <title>Overview</title>
    <para>
  Welcome to the <?php echo $package; ?> Framework. The term "Framework" is often misused to describe software libraries. 
  However the traditional use of the term Framework describes an implementation of libraries that manage the flow of control. 
  The <?php echo $package; ?> Framework is a Software Framework in the traditional definition.
    </para>
    <para>
  The <?php echo $package; ?> Framework allows developers to focus directly on writing business logic. 
  Most PHP Frameworks are intended to facilitate web application (or website) development. 
  However the <?php echo $package; ?> Framework was designed with an extra level of indirection, 
  allowing the same code base to support web applications, web services, command line applications, 
  and provide a concise interface for supporting other implementations.
    </para>
    </refsect1>
    <refsect1 id="{@id software_framework}">
    <title>Software Framework</title>
    <para>
    The framework separates business logic, user interface and data by implementing a software architecture pattern known as Model-view-controller (MVC).
    </para>  
    <para>
  MVC is a popular architectural design pattern dating back to the 1970s. Variants (and misinterpretations) of this pattern have dominated OOA/D; however, the core value of separating presentation, business logic and data are fundamental to large scale software implementation and maintainability.
    </para>
    <refsect2 id="{@id flow_of_control}">
    <title>Flow of Control</title>
    <para>
The flow of events through the framework starts at the Web or 
Cli Framework classes. The process begins when an appropriate 
framework is instantiated by the environment. 
    </para>
    <para>
    <graphic src="../../images/core/stack.png"></graphic>
    </para>
    <para>
  In a web environment the server calls the php 
  script {@link http://<?php echo $webserver . $webPath; ?>docs/<?php echo $package; ?>/server/_www---index.php.html www/index.php}, 
to instantiate the {@link WebFramework} class. The command line environment 
uses {@link http://<?php echo $webserver . $webPath; ?>docs/<?php echo $package; ?>/utilities/_utils---cli.php.html utils/cli.php} 
to instantiate the {@link CliFramework} class. Both 
frameworks extend the core Framework class by providing environment 
specific adaptors to incoming data known as {@link Payload} and persistent 
client side data storage known as {@link ClientData}. The core {@link Framework} 
class provides a common dispatch method for loading controllers 
and calling actions.
    </para>
    <para>
After a framework object is created, the dispatch method is called. 
The dispatch method instantiates a custom controller class, provides 
ClientData and Payload objects to the controller, instantiates a 
default Renderer and invokes a method known as an Action.
    </para>
    <para>
An action is simply a method within a {@link Controller} class object prefixed 
to indicate that it is accessible to a specific framework 
(web_, cli_, etc...) Actions are intended to be entry points to 
specific application logic contained in {@link Model}s and {@link Plugin}s. The bulk 
of application logic should be encapsulated in Plugins. 
    </para>
    <para>
Actions are intended to load the appropriate plugins, passing data 
from {@link Payload} and {@link ClientData} and retrieving data back from the plugins. 
Data retrieved from {@link Plugin}s and {@link Model}s is passed to the {@link Renderer} along 
with optional meta data. The XHTMLRenderer requires a View (a php 
script functioning as a template,) yet other {@link Renderer}s such as 
{@link JSONRenderer} and {@link XMLRenderer} do not require views. 
The {@link Renderer} is responsible for data output.
    </para>

    </refsect2>
  </refsect1>

</refentry>
