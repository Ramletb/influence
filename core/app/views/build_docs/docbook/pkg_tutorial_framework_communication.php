<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Framework Communication Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_framework_communication}">
  <refnamediv>
    <refname>Framework Communication</refname>
    <refpurpose></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_framework_communication_key_concepts}">
      <title>Key Concepts</title>
      <para>
Cross-Framework communication is extremely easy using Framework Service Models. 
The remote Framework must have discovery turned on for the controller you wish 
to model. The remote Framework<?php // ?>'s Actions must be properly documented. 
      </para>
    </refsect1>    
    <refsect1 id="{@id tutorial_framework_communication_configure}">
      <title>Configure</title>
      <para>
The best way to demonstrate Framework communication is to utilize our HelloController.
      </para>
      <para>
All controllers are "discoverable" while the Framework is in development mode. 
However, if we are generating models against a live controller we need to turn 
discovery on for each controller we wish to allow.
      </para>
      <para>
Open your custom_application.ini and add the following config block:
      </para>
    <para>
    <screen>
[DISCOVERY]

controller.hello = true
    </screen>
    </para>
    <para>
In order to test discovery on our HelloController we can go to the following url:
    </para>
    <para>
    <browserurl>
    http://<?php echo $webserver; ?>/hello/discover
    </browserurl>
    </para>
    <para>
You should see similar output.
    </para>
    <para>
    <browserscreen>
    <graphic src="../../images/core/hello_actions.png"></graphic>
    </browserscreen>
    </para>
    <para>
We will now configure our Framework to generate Models from 
itself. We would typically be generating Models off of a remote 
Framework, however the concept is the same.
    </para>
    <para>
Open your custom_environment.ini and add the folling config block:
    </para>
    <para>
    <screen>
[HELLOREMOTE]
service.class = "Action"
service.host  = "http://<?php echo $webserver; ?>"
    </screen>
    </para>
    </refsect1>    
    <refsect1 id="{@id tutorial_framework_communication_generating}">
    <title>Generating Service Models</title>
    <para>
Generating Service Models off of our local Framework is the same 
for remote Frameworks. Run the following command.
    </para>
    <para>
    <screen>
$ cd ./utils/; ./cli.php --action build_models --package HELLOREMOTE --header ./doc/SAMPLE.header
    </screen>
    </para>
    <para>
If all goes well you should get the following result:
    </para>
    <para>
    <screen>
Creating ../app/models/HELLOREMOTE/HelloModel.php...
    </screen>
    </para>
    </refsect1>    
    
</refentry>

