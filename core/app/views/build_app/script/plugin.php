<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * plugin.php
 *
 * View for rendering a Plugin.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

/** 
 * <?php echo $data->name; ?>Plugin.php
 * 
 * Contains the {@link <?php echo $data->name; ?>Plugin} class.
 * 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage plugins
 */ 

/**
 * <?php echo $data->name; ?>Plugin class
 *
 * @package <?php echo $data->packageName; ?> 
 * @subpackage plugins
 */
class <?php echo $data->name; ?>Plugin extends Plugin {

  /**
   * Do Something
   *
   */
  public function doSomething() {

  }

  
}
