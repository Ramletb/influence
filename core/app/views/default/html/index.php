<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * index.php
 *
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage views
 */
?>
<!DOCTYPE html>
<html>
<body>
<center>v<?php echo $configuration->getFrameworkFullVersion(); ?></center> 
</body>
</html>



