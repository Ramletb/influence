<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * odModel.php
 *
 * View for rendering an odModel.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

/** 
 * <?php echo $data->packageName; ?>_<?php echo $data->name; ?>Model.php
 * 
 * Contains the {<?php echo $data->packageName; ?>_<?php echo $data->name; ?>Model} class.
 * (this file may be modified safely)
 * 
 * @version <?php echo '$R' . 'ev: $'; ?> 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */ 

/**
 * Requires structure
 */
require_once('<?php echo $modelPath . $data->packageName; ?>/generated/<?php echo $data->packageName . '_' . $data->name; ?>Structure.php');

/**
 * <?php echo $data->name; ?>Model class
 *
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */
class <?php echo $data->packageName; ?>_<?php echo $data->name; ?>Model extends <?php echo $data->packageName; ?>_<?php echo $data->name; ?>Structure {
  
}
