<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * model.php
 *
 * View for rendering a Model.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

/** 
 * <?php echo $data->modelName; ?>Model.php
 * 
 * Contains the {<?php echo $data->modelName; ?>Model} class.
 * (this file may be modified safely)
 * 
 * @version <?php echo '$R' . 'ev: $'; ?> 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */ 

/**
 * Requires structure
 */
require_once('<?php echo $requirePath . $data->packageName; ?>/generated/<?php echo $data->modelName; ?>Structure.php');

/**
 * <?php echo $data->modelName; ?>Model class
 *
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */
class <?php echo $data->modelName; ?>Model extends <?php echo $data->modelName; ?>Structure {
  
}
