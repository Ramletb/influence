<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ErrorController.php
 *
 * Contains the {@link ErrorController} class.
 *
 * @author Brian Hull <brisn.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage controllers
 */

/**
 * The ErrorController Class
 *
 * @package SP5
 * @subpackage controllers
 */
class ErrorController extends Controller {

  /**
   * preAction
   *
   */
  public function preAction() {

    $this->errorCode = array();
    $this->errorCode['403'] = 'Forbidden';
    $this->errorCode['404'] = 'File Not Found';
    $this->errorCode['500'] = 'Internal Server Error';

    if($this->errorCode[$this->actionName]) {

      $this->logger->error($this->actionName . " ACTION CALLED FOR: [". $_SERVER['REQUEST_URI'] ."]");
      $data = new StdClass;
      $data->errorCode    = $this->actionName;
      $data->errorMessage = $this->errorCode[$this->actionName];
      $data->requestURI   = $_SERVER['REQUEST_URI'];
      $this->setData($data);

      return 'error';
    }
  }

  /**
   * error
   *
   */
  public function error() {
    header("HTTP/1.0 ". $this->actionName ." ". $this->errorCode[$this->actionName]); 
    header("Status: " . $this->actionName ." ". $this->errorCode[$this->actionName]);

    $errorView = $this->configuration->application['FRAMEWORK']['default']['error'];
    $errorView = $errorView ? $errorView : 'core/error';
    $this->render($errorView);
  }

}
