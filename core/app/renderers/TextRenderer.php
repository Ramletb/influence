<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * TextRenderer.php
 *
 * Contains the {@link TextRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The TextRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class TextRenderer extends PresentationRenderer {

  /**
   * Render
   *
   * Render text.
   *
   * @access public 
   */
  public function render() {
    if(is_array($this->data) || is_object($this->data)) {
      print_r($this->data);
    } else {
      echo $this->data;
    }

    echo "\n";
  }

  /**
   * Content Type
   *
   * @access public 
   */
  public function contentType() {
    return "content-type: text/plain";
  }


}
