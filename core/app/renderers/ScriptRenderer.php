<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ScriptRenderer.php
 *
 * Contains the {@link ScriptRenderer} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The ScriptRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class ScriptRenderer extends PresentationRenderer {

  /**
   * Render
   *
   * Render a DocBook view.
   *
   * @access public 
   */
  public function render($view=null) {
    $this->loadView($view);
  }

}

?>
