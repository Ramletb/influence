<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * KMLRenderer.php
 *
 * Contains the {@link KMLRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The KMLRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class KMLRenderer extends PresentationRenderer {

  /**
   * Render
   *
   * Render KML
   *
   * @access public 
   * @param string $view an optional view parameter.
   */
  public function render($view=null) {
    header("content-type: text/xml");

    $this->loadView($view);
  }


}

?>
