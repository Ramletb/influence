<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * JSONRenderer.php
 *
 * Contains the {@link JSONRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 9 $
 * @package SP5
 * @subpackage core
 */

/**
 * The JSONRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class JSONRenderer extends SerializerRenderer {

  /**
   * Render
   *
   * Render JSON.
   *
   * @access public 
   */
  public function render() {
    if(array_key_exists('HTTP_X_FRAMEWORK_JSON_REQUEST', $_SERVER) && $_SERVER['HTTP_X_FRAMEWORK_JSON_REQUEST'] == 'PLAINJSON' || $_SERVER['REQUEST_METHOD'] == 'POST') {
      echo json_encode($this->data);
    } else {
      echo '//' . json_encode($this->data);
    }
    echo "\n";
  }

  /**
   * Content Type
   *
   * @access public 
   */
  public function contentType() {
    return "content-type: text/plain";
  }
  

}

?>