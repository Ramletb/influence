<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * XMLRenderer.php
 *
 * Contains the {@link XMLRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require XML/Serializer.php
 *
 * Requires the XML/Serializer class.
 */
require_once('XML/Serializer.php');

/**
 * The XMLRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class XMLRenderer extends SerializerRenderer {

  /**
   * Render
   *
   * Render XML.
   *
   * @access public 
   */
  public function render() {

    $options = array(XML_SERIALIZER_OPTION_INDENT        => '    ',
		     XML_SERIALIZER_OPTION_RETURN_RESULT => true);

    $serializer = &new XML_Serializer($options);

    $result = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    $result .= $serializer->serialize($this->data);

    echo $result . "\n";
  }

  /**
   * Content Type
   *
   * @access public 
   */
  public function contentType() {
    return "content-type: text/xml";
  }

}


?>
