<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * DatabaseFactory.php 
 *
 * Contains the {@link DatabaseFactory} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The DatabaseFactory Class
 *
 * @package SP5
 * @subpackage core
 */
final class DatabaseFactory extends Core {

  /**
   * Constructor
   * 
   * prevents direct instantiation.
   * 
   * @ignore
   * @access protected
   */  
  protected function __construct() {
    parent::__construct();
  }

  /**
   * Instance
   *
   * Holds a DatabaseFactory instance
   * the Database Class
   *
   * @var DatabaseFactory $instance
   * @access private
   * @static
   */  
  private static $instance;

  /**
   * Get DatabaseFactory
   *
   * Get a singleton instance of the DatabaseFactory class.
   * 
   * Example:
   * <pre>
   * $databaseFactory = DatabaseFactory::getDatabaseFactory();
   * </pre>
   *
   * @access public
   * @returns DatabaseFactory a DatabaseFactory object.
   */  
  public static function getDatabaseFactory() {
    if (!isset(self::$instance)) {
      $c = __CLASS__;
      self::$instance = new $c;
    }

    return self::$instance;
  }

  /**
   * Load
   *
   * load is a static method used to return a configured 
   * instances of the Database class.
   *
   * Example:
   * <pre>
   * $testDatabase = $databaseFactory->load("TEST"); // loads Databse from [TEST]
   * </pre>
   *
   * @see Database
   * @param string $package Package name from environment ini
   * @access public
   */
  final public function load($package) {
    try {
      $prefix    = $this->configuration->environment[$package]['db']['class'];
      $suffix    = "Database";
      $classFile = $prefix . $suffix . '.php';
      $className = $prefix . $suffix;

      $baseClassFile = $suffix . '.php';

      require_once($baseClassFile);
      require_once($classFile);
      $databaseObject = new $className;
      $databaseObject->type   = $prefix;
      $databaseObject->dbConfig = $this->configuration->environment[$package]['db'];
      
      return $databaseObject;
    } catch (Exception $e) {
      $this->logger->error("[" . $package . "]" . " DatabaseFactory EXCEPTION - Exception: ".print_r($e,1));
      return false;
    }
  }
  
}

