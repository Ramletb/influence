<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Factory.php 
 * 
 * Contains the {@link Factory} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Factory Class
 *
 * @package SP5
 * @subpackage core
 */
class Factory extends Core {

  /**
   * Instance
   *
   * Holds an array of onfigured instances of Factory
   *
   * @see Factory
   * @var Array $instance
   * @access private
   * @static
   */  
  private static $instance = array();

  /**
   * Type
   *
   * Holds the Factory type
   *
   * @var string $type
   * @access private
   */  
  private $type;

  /**
   * Type Path
   *
   * Holds an array of paths to derived classes of type
   *
   * @var array $typePaths
   * @access private
   */  
  private $typePaths = array();

  /**
   * Get Factory
   *
   * Get a singleton instance of a Factory class configured to
   * produce objects of a specific type.
   * 
   * Example:
   * <pre>
   * $paths = array(C_PATH_PLUGINS, C_PATH_CORE_PLUGINS);
   * $pluginFactory = Factory::getFactory('Plugin', $paths);
   * </pre>
   *
   * @access public
   * @param string $type (Controller, Model, Parser, etc..)
   * @param array $typePaths (Paths to subclasses of type)
   * @returns Factory a {@link Factory} class object.
   */  
  public static function getFactory($type,$typePaths) { 
    if (!isset(self::$instance[$type])) {
      $c = __CLASS__;
      self::$instance[$type] = new $c;
      self::$instance[$type]->type     = $type;
      self::$instance[$type]->typePaths = $typePaths;
    }

    return self::$instance[$type];
  }

  /**
   * Load
   *
   * Load is a static method used to return an instance
   * related to the type of {@link Factory} class instanciated.
   *
   * Example:
   * <pre>
   * $testPlugin = $pluginFactory->load("Test"); // loads TestPlugin.php
   * $testParser = $parserFactory->load("Test"); // loads TestParser.php 
   * </pre>
   *
   * @param string $prefix
   * @param string $package
   * @access public
   */
  final public function load($prefix, $package=null) {
    try {
      $suffix = $this->type;
      $paths = $this->typePaths;
      $path  = '';

      if ($package) {

        // Look first to see if prepended file exists.
	//
	foreach($this->typePaths as $path) {
	  $prepend_file = $path . $package . '/' . $package . '_' .$prefix . $suffix . '.php';
	  $file = $path . $package . '/' . $prefix . $suffix . '.php';
	  $this->logger->trace("Looking for file: ". $file);
	  $prepend = file_exists($prepend_file) ? TRUE : FALSE;
	  $file_exists = file_exists($file) ? TRUE : FALSE;
	  if($prepend || $file_exists) { break 1; }
	}

        $classFile = $prepend ? $path . $package . '/' . $package . '_' .$prefix . $suffix . '.php' : $path . $package . '/' . $prefix . $suffix . '.php';
        $className = $prepend ? $package . '_' .$prefix . $suffix : $prefix . $suffix;
      } else {

	// Check for files
	//
	foreach($this->typePaths as $path) {
	  $classFile = $path . $prefix . $suffix . '.php';
	  $this->logger->trace("Now Looking for file: [". $classFile . "]");
	  if(file_exists($classFile)) {
	    break;
	  }
	}

	$className = $prefix . $suffix;

      }
      $baseClassFile = $suffix . '.php';

      if (is_file($classFile)) {
	$this->logger->trace("Factory loading file: [". $classFile ."]");
	require_once($baseClassFile);
	require_once($classFile);
	$object = new $className;
	
	if(is_a($object, 'App')) {
	  $object->setPrefix($prefix);
	  $object->setFilePath($classFile);
	}

	$this->logger->trace("Factory loaded class: [" . $className . "]");
      	
	return $object;
      } else {
	$this->logger->error("[" . $suffix . "]" . " Factory ERROR - File not found: ".$classFile);
      }
    }
    catch (Exception $e) {
      $this->logger->error("[" . $suffix . "]" . " Factory EXCEPTION - Exception: ".print_r($e,1));
      return false;
    }
  }


}

