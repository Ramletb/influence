<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Payload.php
 *
 * Contains the {@link Payload} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require Constants.php
 *
 * Requires constatns contained 
 * in {@link Constants.php}
 *
 */
require_once('Constants.php');

/**
 * Require Data.php
 * 
 * Requires the abstract {@link Data} class
 * contained in {@link Data.php}.
 */
require_once('Data.php');

/**
 * Require DataPackage.php
 * 
 * Requires the {@link PayloadPkg} class
 * contained in {@link DataPackage.php}.
 */
require_once('DataPackage.php');

/**
 * The Payload Class
 *
 * See the Quikstart Guide's {@link local:docs/r.php?t=quickstart&s=helloworld.payload Payload Example}.
 *
 * @package SP5
 * @subpackage core
 */
final class Payload extends Data {

  /**   
   * Constructor
   * 
   * Payload constructor. 
   * Prevents direct instantiation.
   * 
   * @ignore
   * @access protected
   */  
  protected function __construct() {
    parent::__construct();
  }

  /**
   * Instance
   * 
   * Instance is used to ensure a singleton payload object.
   *
   * @var Array $instance
   * @static
   */
  private static $instance;

  /**
   * Data
   * 
   * The data class variable
   *
   * @var array $data hold a hash of payload data
   * @access private
   */  
  private $data = array();

  /**
   * Get Payload
   * 
   * Get a Payload object of a specific type
   *
   * @access public
   * @static
   * @returns Payload a {@link Payload} class object.
   */  
  public static function getPayload() {

    if (self::$instance == null) {
      self::$instance = new Payload();      
    }

    return self::$instance;
  }

  /**
   * Set Data
   * 
   * Send data to a Payload
   *
   * @access public
   * @param string $type type of payload data
   */  
  public function setData($type, $data, $stripslashes=false) {

    if (self::$instance == null) {
      self::getPayload();
    }

    
    foreach($data as $key => $val) {
      if($stripslashes) {
	self::$instance->data[$type][$key] = new PayloadPkg($key,stripslashes($val));
	self::$instance->data['ALL'][$key] = new PayloadPkg($key,stripslashes($val));
      } else {
	self::$instance->data[$type][$key] = new PayloadPkg($key,$val);
	self::$instance->data['ALL'][$key] = new PayloadPkg($key,$val);
      }
    }

  }

  /**
   * Get Data
   *
   * Get all data from the payload.
   *
   * @access public
   * @param string $from type of payload data
   * @returns array $payload_data
   */  
  public function getData($from='ALL') {
    $hash = array();

    foreach($this->data[$from] as $k => $dataPackage) {
      $hash[$k] = $dataPackage->getData();
    }

    return $hash;
  }

  /**
   * Get Data Filtered
   *
   * Get all data from the payload - except the standard keys.
   *
   * @access public
   * @param string $from type of payload data
   * @returns array $clean_data
   */
  public function getDataFiltered($from='ALL') {
    return $this->filter($this->getData($from));
  }

  /**
   * Get Param
   *
   * Get a parameter from the payload. 
   *
   * @deprecated deprecated since version 2.1 see getPackage
   * @access public
   * @param string $param param of payload data
   * @param string $from type of payload data
   * @returns mixed $value
   */  
  public function getParam($param,$from='ALL') {
    if(array_key_exists($from, $this->data) && array_key_exists($param, $this->data[$from])) {
      $dataPackage = $this->data[$from][$param];
      if($dataPackage) {
	return $dataPackage->getData();
      }
    }
  }

  /**
   * Get Package
   *
   * Get a {PayloadPkg} from the payload. 
   *
   * @see DataPackage
   * @access public
   * @param string $param param of payload data
   * @param string $from type of payload data
   * @returns PayloadPkg
   */
  public function getPackage($param,$from='ALL') {
    return $this->data[$from][$param] ? $this->data[$from][$param] : new PayloadPkg($param,null);
  }

  /**
   * Get Array Package
   *
   * Get a {PayloadArrayPkg} from the payload. 
   *
   * @see DataPackage
   * @access public
   * @param string $param param of payload data
   * @param string $from type of payload data
   * @returns PayloadArrayPkg
   */  
  public function getCollectionPackage($param,$from='ALL') {
    $arrayPkg = new PayloadCollectionPkg($param,null);
    foreach($this->data[$from] as $pkg) {
      if(preg_match('/^' . $param . '/', $pkg->getName())) {
	$arrayPkg->addPackage($pkg);
      }
    }
    return $arrayPkg;
  }

  /**
   * Filter
   *
   * Filter the standard payload keys from the data.
   *
   * @access private
   * @param array $data to be cleaned.
   * @returns array $clean_data
   */
  private function filter($data) {
    $dirty = $this->configuration->framework['PAYLOAD_FILTER'];  

    $clean = array();

    foreach (array_keys($data) as $k) {
      if (!$dirty[$k]) { $clean[$k] = $data[$k]; }
    }

    return $clean;
  }

}
