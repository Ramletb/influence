<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Data.php 
 *
 * Contains the abstract {@link Data} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require Encryptor.php
 *
 * Requires the {@link Encryptor} class
 * contained in {@link Encryptor.php}.
 */
require_once('Encryptor.php');

/**
 * The Abstract Data Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class Data extends Core {

  /**
   * Encryptor
   *
   * Holds an {@link Encryptor} object
   *
   * @var Encryptor $encryptor
   * @uses Encryptor::getEncryptor()
   * @access protected
   */  
  protected $encryptor;

  /**
   * Constructor
   * 
   * @ignore
   * @access protected
   */  
  protected function __construct() {
    parent::__construct();
    $this->encryptor = Encryptor::getEncryptor();
  }

  /**
   * Prepare String
   *
   * Prepare a string.
   *
   * @param string $stringData
   * @param bool $addSlashes
   * @access public
   * @returns string
   */
  public function prepareString($stringData, $addSlashes=TRUE) {
    $stringData = $addSlashes ? addslashes($stringData) : $stringData;

    return "'" . $stringData . "'";
  }

  /**
   * Prepare Number
   *
   * Prepare a numeric value.
   *
   * @param number $numberData
   * @access public
   * @returns number
   */
  public function prepareNumber($numberData=0) {
    return  $numberData + 0 ;
  }

  /**
   * Prepare Geometric Type (point, line, etc)
   *
   * Prepare a geometric value.
   *
   * @param string $geometricData
   * @access public
   * @returns string
   */
  public function prepareGeometric($geometricData) {
    $geometricData = $addSlashes ? addslashes($geometricData) : $geometricData;
    return $geometricData;
  }

  
}
