<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * MySQLDatabase.php
 *
 * Contains the {@link MySQLDatabase} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */


/**
 * The MySQLDatabase Class
 *
 * @package SP5
 * @subpackage core
 */
class MySQLDatabase extends Database {

  /**
   * Constructor
   * 
   * MySQLDatabase constructor is called when a 
   * database is instantiated by a databaseFactory.
   * 
   * @ignore
   * @see Database
   * @see DatabaseFactory
   * @access public
   */  
  public function __construct() {
    parent::__construct();
    $this->logger->trace("Instantiated MySQLDatabase...");
  }

  /**
   * Connect
   * 
   * Create and return a database connection.
   *
   * @access protected
   * @returns string $connection
   */
  protected function connect() {
    $username   = $this->dbConfig['username'];
    $password   = $this->dbConfig['password'];
    $database   = $this->dbConfig['database'];
    $host       = $this->dbConfig['host'];
    $charset    = $this->dbConfig['format']['charset'];
    $charset    = $charset ? $charset : 'utf-8';

    if (isset($this->dbConfig['port'])) {
      $port     = $this->dbConfig['port'];
    } else {
      $port     = 3306; //std mysql port
    }

    // create a connection to the database
    try {
      $this->logger->trace("DBCONFIG CHARSET ENCODING: [".$charset."]");

      if($charset) {
        mb_internal_encoding($charset);
      }

      $dbConnection = new mysqli($host, $username, $password, $database);
      $dbConnection->query("SET NAMES '".$charset."'");

      if (!$dbConnection) {
        throw new Exception ("database connection failure");
      }

      return $dbConnection;
    }

    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Disconnect
   * 
   * Close an existing database connection.
   *
   * @param string $dbConnection
   * @access protected
   */
  protected function disconnect($dbConnection) {
    $dbConnection->close();
  }

  /**
   * Now
   * 
   * Return date-time in format required by DB.
   * This will default to the default format used by the database, but may be overridden in the config.
   *
   * @access public
   * @returns string $current_datetime
   */
  public function now() {
    $format = $this->dbConfig['format']['datetime'] ? $this->dbConfig['format']['datetime'] : 'Y-m-d H:i:s';

    return date($format, time());
  }

  /**
   * Execute
   * 
   * Execute a non query operation on the database (typically: insert, update, delete)
   *
   * @param string $statement SQL statement
   * @access public
   * @returns int $result number of database rows affected
   */
  public function execute($statement) {

    try {
      $dbConnection = $this->connect();

      $this->logger->trace($statement);

      $dbConnection->query($statement);

      if ($dbConnection->error) { 
	$this->logger->error($statement); 
	$this->logger->error("db_execute error: " . $dbConnection->error); 
      }

      // hrm.
      $result = $dbConnection->insert_id ? $dbConnection->insert_id : $dbConnection->affected_rows;

      $this->disconnect($dbConnection);

      return $result;
    } catch (Exception $e) {
      $this->disconnect($dbConnection);
      throw $e;
    }
  }

  /**
   * Query
   * 
   * Execute a query operation on the database (typically: select)
   *
   * @param string $statement
   * @access public
   * @returns array $result_set
   */
  public function query($statement) {
    try {
      $dbConnection = $this->connect();

      $this->logger->trace($statement);

      $resultHandle = $dbConnection->query($statement);

      // return any data in requested format
      $result = array();

      if ($dbConnection->error) { $this->logger->error("db_query error: " . $dbConnection->error); }

      if ($resultHandle) {
        while ($row = $resultHandle->fetch_assoc()) {
          array_push($result, $row);
        }
      }

      $this->disconnect($dbConnection);

      return $result;
    } catch (Exception $e) {
      $this->disconnect($dbConnection);
      throw $e;
    }
  }

  /**
   * Smart Insert
   * 
   * Execute an insert operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $valuesHash
   * @access public
   * @returns array $result
   */
  public function smartInsert($model, $valuesHash) {
    $dbargs = $model->prepare($valuesHash);

    $qry = "INSERT INTO `" . $model->getTableName() . "` (";

    foreach ($dbargs as $key => $val) {
      if ($colnames) { $colnames .= ','; }
      $colnames .= '`' . $key . '`';
    }

    $qry .= $colnames . ') VALUES (';

    foreach ($dbargs as $key => $val) {
      if (strlen($values) > 0) { $values .= ','; }
      $values .= $val;
    }

    $qry .= $values . ');';

    return $this->execute($qry);
  }

  /**
   * Smart Update
   * 
   * Execute an update operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $setHash
   * @param array $whereHash
   * @param string $addendum
   * @access public
   * @returns array $result
   */
  public function smartUpdate($model, $setHash, $whereHash,$addendum='') {
    $dbset = $model->prepare($setHash);
    $dbwhere = $model->prepare($whereHash);
    $qry = 'UPDATE `' . $model->getTableName() . '` SET ';

    foreach ($dbset as $key => $val) {
      if ($setqry) { $setqry .= ', '; }
      $setqry .= '`' . $key . '`' . '=' . $val;
    }

    $qry .= $setqry;

    if (count($dbwhere) > 0) {
      $qry .= ' WHERE ';

      foreach ($dbwhere as $key => $val) {
        if ($whereqry) { $whereqry .= ' AND '; }
        if (substr($key,-1)=='!') {
          $key = substr($key,0,strlen($key)-1);
          $whereqry .= '`' . $key . '`' . ' <> ' . $val;
        } else {
          $whereqry .= '`' . $key . '`' . '=' . $val;
        }
      }

      $qry .= $whereqry;
    }

    $qry .= ' ' . $addendum . ';';

    //$this->logger->debug($qry);

    return $this->execute($qry);
  }

  /**
   * Smart Delete
   * 
   * Execute a delete operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $whereHash
   * @access public
   * @returns array $result
   */
  public function smartDelete($model, $whereHash) {
    $dbwhere = $model->prepare($whereHash);
    $qry = 'DELETE FROM `' . $model->getTableName() . '` ';

    $qry .= $setqry;

    if (count($dbwhere) > 0) {
      $qry .= ' WHERE ';

      foreach ($dbwhere as $key => $val) {
        if ($whereqry) { $whereqry .= ' AND '; }
        if (substr($key,-1)=='!') {
          $key = substr($key,0,strlen($key)-1);
          $whereqry .= '`' . $key . '`' . ' <> ' . $val;
        } else {
          $whereqry .= '`' . $key . '`' . '=' . $val;
        }
      }

      $qry .= $whereqry;
    }

    $qry .= ';';

    $this->execute($qry);
  }

  /**
   * Smart Select
   * 
   * Execute a select operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $whatHash
   * @param array $whereHash
   * @param string $addendum
   * @access public
   * @returns array $result
   */
  public function smartSelect($model, $whatHash, $whereHash, $addendum) {
    if ($whereHash != null) {
      $whereHash = $model->prepare($whereHash);
    }

    $qry = 'SELECT ';

    if ($whatHash && count($whatHash) > 0) {
      foreach ($whatHash as $key) {
        if ($whatqry) { $whatqry .= ', '; }
        $whatqry .= '`' . $key . '`';
      }

      $qry .= $whatqry;
    } else {
      $qry .= '*';
    }

    $qry .= ' FROM `' . $model->getTableName() . '`';

    if ($whereHash && count($whereHash) > 0) {
      foreach ($whereHash as $key => $val) {
        if ($whereqry) { $whereqry .= ' AND '; }
        $whereqry .= '`' . $key . '`' . '=' . $val;
      }

      $qry .= ' WHERE ' . $whereqry;
    }

    $qry .= ' ' . $addendum;

    $qry .= ';';

    return $this->query($qry);
  }

  /**
   * Smart Select Paginated
   *
   * Execute a paginated select operation on the DB based on supplied args.
   *
   * @param Model $model
   * @param array $whereHash
   * @param array $orderHash
   * @param int $page
   * @param int $perPage
   * @access public
   * @returns array $result
   */
  public function smartSelectPaginated($model, $page, $perPage, $whereHash=null, $orderHash=null) {

    $countQuery  = "SELECT COUNT(1) as total FROM " . $model->getTableName();
    if($whereHash != null) {
      $countQuery .= $model->whereString($whereHash, ' WHERE');
    }

    $page += 0;
    $perPage += 0;

    $countResult = $this->query($countQuery);
    $count       = $countResult[0]['total'];
    $count       = $count ? $count : 1;

    $pageNumber  = ($page - 1);
    $recordsIn   = ($perPage * $pageNumber);

    $dataQuery   = "SELECT * FROM " . $model->getTableName();

    if($whereHash != null) {
      $dataQuery  .= $model->whereString($whereHash,' WHERE');
    }

    if($orderHash != null) {
      $dataQuery  .= $model->orderString($orderHash, ' ORDER BY');
    }

    $dataQuery  .= " LIMIT " . $recordsIn . ',' . $perPage;
    
    return array('totalRecords' => $count,
		 'totalPages'   => ceil($count / $perPage),
		 'page'         => ($pageNumber + 1),
		 'perPage'      => $perPage,
		 'pageRecords'  => $this->query($dataQuery));
  }

  /**
   * Smart Select Conditions
   *
   * @param Model $model
   * @param array $whereHash
   * @param array $orderHash
   * @access public
   * @returns array $result
   */
  public function smartSelectConditions($model, $whereHash=null, $orderHash=null) {

    $dataQuery   = "SELECT * FROM " . $model->getTableName();

    if($whereHash != null) {
      $dataQuery  .= $model->whereString($whereHash,' WHERE');
    }

    if($orderHash != null) {
      $dataQuery  .= $model->orderString($orderHash, ' ORDER BY');
    }
    
    return $this->query($dataQuery);
  }

}

?>
